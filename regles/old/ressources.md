# Ressources

## Dépense de ressources
carte main y compris objectifs, génothèque, pion puits

## Pions extracteurs
Les tribus possèdent des pions extracteurs de différentes valeurs (1, 2 ou 3). Ils sont conservés par la tribu mais n'ont aucun effet tant qu'ils ne sont pas en jeu. Ils n'ont pas de couleur, juste un symbole de la tribu.

## Pions puits

En début de partie (Mise en place), on place en jeu les pions puits : 4 / couleur, on en place 2 / plateau.

En commençant par le régisseur, chaque joueur en place un à tour de rôle sur une case vide neutre.

## Récolte

L'action récolte consiste à réaliser une activation sur un pion puits. On remplace alors ce pion puits par un pion extracteur de sa tribu.

Lorsqu'on pose un pion extracteur, on retire immédiatement un pion flaune en jeu au choix (s'il en reste).

Il n'est pas possible de faire une récolte qui nous ferait dépasser notre maximum de pions puits déterminé par nos Bâtiments : chaque bâtiment indique un nombre de pions puits pour chaque couleur. L'échelle de valeur est prévue pour qu'il y ai assez de pions puits en jeu au départ pour que tout le monde en récupère. La flaune ne cassant que les pions extracteurs, pas puits, on est à l'abri.

Les pions extracteurs en jeu donnent des bactéries en fin de partie. Si un pion flaune vient recouvrir un pion extracteur, ça n'implique que la perte du pion bactérie, le pion puits reste à la tribu (surtout qu'il n'y a pas de trace de quel pion puits correspond à quel pion extracteur).
