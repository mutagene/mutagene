# Fin de partie
On score :
* les objectifs validés (Bactéries indiquées au dos de chaque objectif)
* les pions extracteurs que l'on possède en jeu (1 Bactérie / extracteur *{?}*)

Se déclenche :
* si plus de pion flaune en stock : lorsqu'on pose le dernier, fin de la partie à la fin du tour.
* si un joueur n'a plus de pioche ni de défausse (il peut avoir des cartes en main) : fin de la partie à la fin du tour.
* si plus aucune case objectif disponible : que des déploiement ou des pions fouillés dessus : fin de la partie à la fin du tour.
