# Actions
## Mouvement
1 PA pour case adjacente sans clone, 2 PA pour passer par dessus un clone allié (pas adversaires, et pas plus de 1 clone). Pioche événement si case événement ou pion flaune sur la destination.

## Sortir des cuves
1 PA : mettre pion clone des cuves sur un de nos Déploiement​ sans clone. Si allié sur déploiement, peut comme pour mouvement payer 1 PA en tout pour aller à 1 mvt terrestre du déploiement (pas plus et pas adversaire).

## Activation
Permet de jouer/poser/valider {*Terminologie à détailler*} les objectifs, de poser un pion extracteur, et divers effets peuvent demander une activation sur telle ou telle case (exemple : mutation). 0 de portée terrestre. Nombre de PA variable (objectif précisé sur la carter, récolte : dépend du pion extracteur qu'on place / retire, cf [ressources](./ressources.md))

## Regression
Action fournie par les mutations greffées sur le clone, jouable en mettant la mutation au charnier (c'est l'effet Burn​ des mutations). 0 PA de base mais coûts variables. cf. Mutations​ pour plus de détails.

## Greffe
cf. Mutations​ pour plus d'infos.
3 PA. La cible doit avoir un emplacement de libre.
On peut greffer une mutation de sa main, ou une mutation greffée au clone actif. On peut également greffer un objectif greffé au clone actif ({*Mais que allié ?*}, et jamais depuis la main) si place disponible.

## Nécrose
3 PA. Permet au clone actif de jeter X dés pour tenter d’infliger des blessures au clone cible. Le clone cible jette {*peut jeter ?*} X dés pour se défendre. X étant la valeur Rouge respective des clones impliqués (pouvant être augmenté avec tout ou partie des dés en Concentration). Chaque jet a pour seuil de réussite la valeur Rouge du clone adverse. La soustraction des réussites de l’attaque par la défense indique le nombre de blessures infligés au clone cible.

## Soin
3 PA, cible clone à [Bleu clone actif] cases bio. [Bleu clone actif] Dés à [Bleu clone cible] SR, sans défense. Priorité restauration de PA, puis si tous les PA sont restaurés, soigne (décale le pion Blessure).

## Mêlée
1 PA, cible clone à 1 case terrestre, clone actif et cible font [Vert ce clone] Dés à [Vert autre clone] SR. Chaque réussite dépense 1 PA, si plus de PA dispo fait 1 Blessure.

## Concentration
2 PA. [Vert clone actif] Dés à [Vert clone actif] SR. Chaque réussite donne 1 Dé en concentration (max : (Vert] dés).
Les dés en concentration peuvent être utilisés (tous ou une partie) à chaque fois que le clone a un jet de dés à faire. Ils sont tous perdus à la première blessure subie. Ils peuvent être utilisés également pour une concentration, le maximum ne changeant pas.

## Onde de choc
3 PA, cible clone à [Bleu clone actif] cases terrestres. {*Cible se défend ?*} Bleu dés, etc ... Chaque réussite déplace la cible d'une case terrestre au choix du clone actif, mais toute pioche d' Événements et Flaune​ arrête le truc (on peut pas faire d'aller retours sur évenement, le premier stop tout).
