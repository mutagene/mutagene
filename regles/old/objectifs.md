# Objectfs

## Fonctionnement général

Chaque tribu possède des cartes objectifs aux symboles (au dos de la carte notamment) qui correspondent aux symboles objectifs sur les plateaux de jeu. Lors de la Mise en place, chaque tribu sélectionne les objectifs des plateaux utilisés pour la partie afin de les mélanger son deck.

Chaque case objectif possède en début de partie un pion objectif au symbole correspondant.

Lorsqu'un clone active un pion objectif pour 'débuter' une mission, on défausse le pion.

Chaque carte objectif indique sont fonctionnement pour être validé et en gagner les Bactéries (points de victoire) qu'il rapporte : on prend un nombre de pions bactérie pour marquer le gain de points (qui peut être variable pour un même objectif).

Certains objectifs vont demander à ce qu'on le greffe au clone actif : ce n'est possible que sur un emplacement qui le permet (cf Clones). *{On peut défausser une mutation si elle occupe l'emplacement d'objectif ?}* L'objectif greffé n'est pas encore validé, et en cas de mort du clone il ira à la défausse de la tribu qui en est propriétaire.

Pour être validé, il faut en général que le clone auquel l'objectif est greffé fasse une activation (souvent sur déploiement de sa tribu, ou adverse), ou alors remplisse une condition spéciale (genre truc automatique : faire X blessures, soigner X, flaune, etc).

L'usage des pions Bactérie permet de faire un compteur sur la carte, même si on gagne pas obligatoirement la même quantité en validant. Ex:
```
"Condition" : posez 1 pion. Activez sur  si 4 pions : validez et scorez en 2."
```
*{pas forcément le plus élégant mais au moins on peut moduler le compteur}*. Après on peut très bien utiliser les pions flaune, ce qui entraîne un deuxième effet kiss-kool sur les fins de parties : l'objectif peut plus être scoré à partir d'un certain nombre et précipiter la fin. *{D'autres pions peuvent être imaginés mais je sais pas si c'est vraiment nécessaire.}*

## Règles

Effet pour greffer l'objectif au clone. L'emplacement objectif sur la plaque de clone doit être disponible. -> l'objectif est greffé au clone et le pion objectif est placé dessus.

Effet pour valider (scorer) l'objectif greffé.

À la mort d'un clone, son éventuel objectif greffé est défaussé, tandis que le pion objectif correspondant est placé sur la case qu'occupait le clone.

## Notes :

Un objectif a en général un effet de greffe subtil et un effet de score simple ou le contraire (greffe simple, score subtil).
