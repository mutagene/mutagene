# Burn
Effet de jouer une carte pour un effet spécial dit "effet Burn" et en mettant la carte dans le charnier au lieu de la défausse. Fonctionnement différent selon le type de carte.

## Carte de tribu
Effet indiqué dans la deuxième moitié du bloc "texte de règle de la carte". Prix différent précisé, et effet souvent plus fort (ou alors prix réduit pour le même effet). En théorie usage unique car le charnier n'est pas remélangé à la pioche comme la défausse, mais différents effets permettent ponctuellement de récupérer des cartes du charnier.

## Objectifs
Par défaut les objectifs n'ont pas d'effet Burn indiqué sur la carte, mais chaque Bâtiment propose (côté QG) un effet au prix du burn d'un objectif en main.

## Mutations
L'effet Burn des mutations est dénommé Régression (cfActions ), et ne fonctionne que lorsque la mutation est greffée {*sûr ?*}
