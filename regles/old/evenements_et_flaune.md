# Événements et Flaune
*30 pions flaune ou 10/ joueurs ?*

Lorsqu'on pioche une carte événement en jouant son effet flaune (que ce soit via un pion flaune ou via une case de la même couleur que la carte), le régisseur place immédiatement un pion flaune sur une case adjacente à la notre, ou adjacente au groupement de pion flaune et événements à laquelle appartient notre case. La case doit être vide mais peut avoir un pion extracteur qui est alors défaussé. Clone, puits, etc bloquent la flaune.Les conduits connectent les cases flaunes/événements.

## Idées
Certaines cartes flaunes peuvent avoir un effet proportionnel au nombre de pions flaunes connectés à cet événement (avec d'éventuels autres cases événement qui font la jonction *{peut être pas les conduits du coup ...}*) : nombre de blessures par exemple ...
