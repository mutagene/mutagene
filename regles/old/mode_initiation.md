# Mode initiation

Le mode initiation consiste à des configurations pré-définies recommandées pour les premières parties de découverte. L'ensemble des règles s'applique, mais on ne garde pas les cartes d'action des clones ainsi que les mutations supplémentaires (celles qui ne sont pas greffées en tout début de partie).

3 Configurations proposées, à la "difficulté" croissante.

Chaque configuration, pour chaque joueur, détermine :
- 2 clones, et leurs mutations de départ *{Nécessairement 2 ? Une seule pour la première config ?}*. Les autres mutations des clones et leurs cartes actions ne sont pas utilisées dans ce mode de jeu.
- Le QG et l'AP

De plus, la disposition des plateaux est indiquées *{Pas nécessairement l'orientation, mais au moins la position afin d'éviter des positions de départ trop proches}*