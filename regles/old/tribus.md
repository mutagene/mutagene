# Tribus
## Matériel
* Pioche de cartes "action"
* Objectifs pour chaque plateau de jeu
* Pions déploiement
* Pions extracteurs
## Main
Main de départ à 5. Effet peuvent changer ça.

Les dos des cartes de notre main sont visibles de tous les joueurs. S'ils demandent, on peut leur montrer en étalant la main ou autre.

On complète sa main à la fin de chaque séquence d'un de nos clones. À la fin de chaque tour, on peut défausse librement des cartes pour recompléter.
## Pioche
Lorsque la pioche est épuisée, on mélange la défausse pour en faire une nouvelle. Si plus de défausse à ce moment là, on déclenche une Fin de partie.
Le dos de la pioche est visible par tous les joueurs.
## Défausse

## Charnier

Le charnier est une sorte de "défausse permanente", mais qu'on conserve à côté de la pioche (*{à l'horizontale pour distinguer de la défausse, au choix}*). Les cartes mises au charnier suite à un effet de Burn ou autre y reste jusqu'à effet spécial pour la récupérer, alors que les cartes de la défausse sont mélangées pour refaire une pioche épuisée.
