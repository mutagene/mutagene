# Bâtiments
Les bâtiments ont 2 faces. Une face "premier bâtiment posé" (Quartier général), avec effet burn et valeur génothèque, etc. Une face "bâtiment modificateur" (Avant poste): le bâtiment est alors posé sous le premier avec une section qui dépasse et ne fait qu'augmenter les valeurs de génothèque / nombre de puits, voire apporte un nouvel effet. Ainsi, on peut poser une vraie différence entre primo-bâtiment et second, et le choix lors du draft est plus riche.

Ils offrent tous :
* Une quantité de puits que l'on peut acquérir {*configuration de couleurs ?*}
* Un effet burn pour les Objectifs même sur un objectif greffé
* L'accès à des "déploiement" à travers les cases de même symbole.

Plus précisément :
* Face QG : valeur puits (max 4), effet burn, pouvoir spécial
* Face AP : valeur puits qui s'ajoute (max 3), effet modificateur de type +/- SR/Dés (majoritairement nécrose) ou taille main
En début de partie, on drafte avec 4 bâtiments par tribu afin d'en garder 3. On choisit 1 bâtiment qu'on pose immédiatement en jeu devant nous face *QG*. On pose les 2 autres sous notre pioche. Lorsqu'on épuise notre pioche pour la première fois, on choisi un des 2 bâtiments posés dessous et on la met en jeu côté *AP* (le dernier restant n'est pas conservé, il peut rejoindre les autres dans la boîte).

Lorqu'un effet nous fait mélanger notre pioche de tribu, on laisse toujours le bâtiment en place, c'est toujours la dernière carte de notre pioche.

Lorsqu'on épuise pour la première fois notre pioche (c'est à dire lorsqu'on voit la carte bâtiment), on pose en jeu l'Avant Poste révélé.
