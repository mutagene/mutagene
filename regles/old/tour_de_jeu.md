# Tour de jeu
## Début du tour (climat ?)
1. Le régisseur pose un pion Flaune sur une case neutre adjacente à un autre pion flaune, ou sur une case vide de son choix si aucun pion flaune en jeu. La case choisie ne doit contenir aucun pion, à l'exception d'un éventuel pion extracteur qui est alors retiré du jeu *{ou recouvert ?}*, et qui ne comptera alors pas en fin de partie.
2. Toutes les cartes en génothèque face cachée sont retournées face visible.

## Fin du tour
1. Chaque joueur retourne ses pions puits sur la face disponible.
2. Les clones restaurent tous leurs PA dépensés.
3. *{quelquechose en rapport avec les cuves}*
4. Le pion régisseur est passé au joueur de droite de l'actuel régisseur.
