# Mise en place

## Détermination du régisseur
Au hasard.

## Choix des tribus
En commençant par le régisseur, chaque joueur choisit une tribu, et en prend la pioche.

## Choix des clones
En commençant par le régisseur, chaque joueur choisit un clone à tour de rôle, jusqu'à ce que chaque joueur ait 3 clones. Ils en prennent ensuite les cartes, pions et plaques.
Les clones sont placés dans un ordre donné, côte à côte *{il y a donc un clone de gauche, un central et un de droite}*

*(Variante possible en draft, à tester, mais il faut que tous les clones soient accessibles)*

Ensuite on choisit les mutations *{Mais à réfléchir : pourrait être bien de le faire après choix des plateau}* : 2 par clone. *{Note : en cas de mutations de tribu, les interdire à la greffe de début de partie ?}*

## Choix des plateau
En commençant par le régisseur, chaque joueur choisit 1 plateau et le pose en respectant les règles de pose, jusqu'à ce qu'on ai posé 6 plateaux.

Chaque joueur prend les cartes Objectifs dont le symbole est présent sur un plateau en jeu et les ajoute à son deck.

## Mise en place des pions puits

## Draft et Pose des bâtiments
*{On place l'AP sous le QG afin de le garder secret}* **->Non, trop compliqué à gérer en cas de mélange de la pioche, on le place dessous sans secret**
