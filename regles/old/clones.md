# Clones
## Mutations
Les emplacements de mutations peuvent abriter un objectif à condition d'avoir un symbole pour l'indiquer.

## Mort d'un clone
Lorsqu'un clone n'a plus de PA (pion PA bloqué entre le pion cuve et le pion blessure), il meurt. Le ou les éventuels Objectfs qui lui seraient greffés sont défaussés dans la défausse de leur tribu, avec le pion objectif qui est placé sur la case occupée par le clone. Les mutations greffées le restent.
