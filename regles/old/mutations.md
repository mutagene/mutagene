# Mutations
En dehors des mutations greffées lors de la mise en place, on ne peut greffer de nouvelles mutations que par l'intermédiaire de l'action greffe *{plus de règle sur la cuve donc}*.

Greffe : lorsqu'un clone réalise une action de greffe, il peut greffer à la cible une mutation de la main de la tribu ou une mutation greffée au clone actif. Le clone cible doit avoir un emplacement libre sinon la greffe n'est pas réalisable.
