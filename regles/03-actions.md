Sortir des cuves :
  - 0 PA
  - Cible case QG
  - Si pas de case libre, on peut choisir une carte adjacente

Mouvement :
  - 1 PA (2 si allié à traverser)
  - Portée terrestre, déclenche évenement / flaune

Mêlée :
  - Vert
  - 1 PA

Concentration :
  - Vert
  - Portée 0
  - 2 PA
  - Donne autant de dés en concentration que les succès (dans la limite du maximum du clone)

Soin :
  - Bleu
  - B cases terrestre
  - 3 PA
  - Décaler pion blessure à gauche, ou le pion PA à droite d'autant que de succès

Onde de choc : effet : déplacez de X cases bio/terrestres, avec pioche d'événement
  - Bleu
  - B cases terrestre
  - 3 PA
  - Déplacer la cible jusqu'autant de cases terrestres que de succès, ne déclenche pas les évenement / flaune

Nécrose :
  - Rouge
  - 3 PA
  - Cible se défend, différence inflige Blessures

Récolte :
  - Rouge
  - 1 / 2 / 3 PA (selon le niveau de l'extracteur placé)
  - prendre 1 puits en jeu en remplaçant par 1 pion extracteur de la réserve de la tribu (dans la limite du max donné par le QP / AP)

Activation
  - 3 PA
  - Greffer objectif au clone en jeu (nécessite emplacement libre) en activant sur case correspondante (symbole ET points)
  - sur case objectif : poser l'AP correspondant depuis la main en jeu si cette tribu n'a aucun AP.
