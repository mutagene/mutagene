# Structure d'un tour

## Début du tour

Poser 4 pions flaune sur le pion Régisseur.

## Séquences

Chaque joueur, en commençant par le Régisseur puis dans le sens anti-horaire, effectue une séquence avec un clone disponible (disposant de PA).

### Structure d'une séquence

- Choix du clone actif

- Actions :

  Réaliser des actions avec le clone actif

- Greffe / Génothèque :

  Possibilité de greffer une carte mutation de la main à un clone allié (en jeu ou pas).

  Possiblité de placer une carte de la main en génothèque

- Repioche :

  Piocher des cartes jusqu'à sa limite de main.

## Fin de tour :

Le Régisseur place en jeu les éventuels pions flaunes sur le pion régisseur puis passe ce dernier à la tribu de droite.