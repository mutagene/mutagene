# Mise en place

## Surface de jeu

* Installation des plateaux (2 par tribu)
* Pose des pions puits (1 de chaque couleur par plateau, sur case vide)
* Pose des pions Objectifs sur les symboles et nombre de points correspondants
* Mélange de la pioche Événement

## Tirage du Régisseur

Au hasard.

## Choix des tribus

En commençant par le régisseur, chaque joueur choisit une tribu :
* Retirer de la pioche les Objectifs qui ne sont pas en jeu
* Prendre tous les pions

## Choix des clones

En commençant par le régisseur, chaque joueur choisit un clone à tour de rôle dans le sens des inverse aiguilles d'une montre, jusqu'à ce que chaque joueur ait 3 clones. Ils en prennent ensuite les cartes, pions et plaques.

Les clones sont placés dans un ordre donné (au choix de la tribu), côte à côte *(il y a donc un clone de gauche, un du centre et un de droite)*.

On greffe à chaque clone 1 de ses mutations, et ses actions et mutations restantes sont mélangées dans la pioche de sa tribu.

## Choix du Quartier Général (QG)

Chaque tribu pioche 3 cartes Zones parmis celles présentes en jeu.

En commençant par le joueur à gauche du Régisseur, puis dans le sens des aiguilles d'une montre, chaque tribu choisit un QG. Il n'est pas possible de choisir un QG de même symbole qu'un autre joueur. Les 2 zones restantes sont placées sous le QG choisi (pour être utilisées en Avant Post).

## Mélange et pioche

Chaque tribu mélange sa pioche et constitue sa main de départ (définie par son QG).