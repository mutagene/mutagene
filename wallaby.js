// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-undef
module.exports = (wallaby) =>  ({

  files: [
    'src/*.ts',
    'src/**/*.ts',
    'config.ts',
    'tsconfig.json',
    'test/**/*.spec.ts',
    'elements/**/*.yml'
  ],

  filesWithNoCoverageCalculated: [
    'src/*/*.interfaces.ts',
  ],

  hints: {
    ignoreCoverageForFile: /ignore file coverage/,
    ignoreCoverage: /ignore coverage/,
  },

  tests: [
    'tests/**/*.spec.ts'
  ],

  env: {
    type: 'node',
    runner: 'node',
  },

  testFramework: 'mocha',

  workers: {  // à voir si nécessaire
    initial: 6,
    regular: 4,
    restart: false,
  }
  // TODO: ok pour wallaby, mais mocha lancé via npm ET ide : nok
  // setup: wallaby => {
  //   global.expect = require('chai').expect;
  // },
})