import { pagine } from 'image-manipulation'
// import { PrintableCard }  from './exportsFromYml'
import * as fs from 'fs'
import { parse } from 'yaml'

const cardsExportFolder: string = process.env.PWD + '/material/cards_exports/'


const allCards =
    fs.readdirSync(cardsExportFolder)
        .map(f => cardsExportFolder + f)
//        .filter(f => f.slice(-9) === 'recto.jpg')





async function main() {

  return await 
    pagine(
      allCards,
//      ...allCards.map((x: PrintableCard) => root + x.back),
    process.env.PWD + '/print/cards/', 'mut')
  
}

main()
  .then(x => console.log(x))
  .catch(console.error)
