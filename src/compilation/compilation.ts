/**
 * Compile les cartes en données complètes, afin que chacun soit compréhensible individuellement :
 *      - remplacement des {{name}}
 *      - remplacement des {{major}}, {{medium}}, {{minor}}
 *      - ajout du type et du "propriétaire" (clone, tribu)
 *      - compéter les cartes tribu action et objectif car elles héritent toute de la carte 'parent' de base
 */
import * as M from '../types/mutagene'
import * as R from 'ramda'


const changePlaceHolder: (placeHolder: string) => (replacement: string) => (text: string) => string =
    placeHolder => replacement => text => text.split(`{{${placeHolder}}}`).join(replacement)


/**
 * Change les occurences {{name}} dans le texte d'une carte par le nom de la carte elle même
 * @param card carte
 * @returns carte traduite
 */
export const changeName: (name: string) => (text: string) => string = name => text => changePlaceHolder('name')(name)(text)


// TODO: faire ça en config ?
export const translatedColors: {[x: string]: string} = {
  green: 'vert',
  blue: 'bleu',
  red: 'rouge',
}

export const translateColors: (c: string) => string = c => translatedColors[c]

/**
 * Parse une carte pour changer les {{major}}, {{medium}, {{minor}}
 * @argument colors dictionnaire de couleurs
 */
export const changeRessources: (colors: {[lvl: string]: string}) => (text: string) => string =
    colors => text => {
      for (const lvl in colors) {
        text = changePlaceHolder(lvl)(translateColors(colors[lvl]))(text)
      }
      return text
    }


// TODO: sortir tout ça
import * as fs from 'fs'
import { parse, stringify } from 'yaml'
/**
 * prend un chemin complet et retourne le contenu
 * @param path chemin du fichier
 * @returns
 */
const readFile: (path: string) => string = path => fs.readFileSync(path, 'utf8')
const parseFile: (path: string) => any = R.pipe(readFile, parse)
/**
 * Export complet du jeu dans un json
 * Permet entre autres de tester les diférences de version en git-diff
 */

const elements: string = process.env.PWD + '/elements/' // TODO: in config ?

const colorDictionnaries: {[color:string] : {[x: string]: string}} = {
  'm': parseFile(`${elements}tribus/green/colors.yml`),
  'n': parseFile(`${elements}tribus/blue/colors.yml`),
  'a': parseFile(`${elements}tribus/red/colors.yml`),
}

const parentActions = parseFile(elements + 'tribus/actions.yml')
const parentObjectives = parseFile(elements + 'tribus/objectives.yml')
// END TODO

/**
 * récupération de la tribu avec un simple parse du set
 */
export const getTribeOfCard: (c: M.TribeCard) => string = c => c.set.split('.')[1]


/**
 * Complète une carte action de tribu avec les infos de la carte parente
 * @param a action
 * @param parents parent actions (all)
 * @returns carte complétée
 */
const completeAction: (a: any, parents: any[]) => M.Action = (a, parents) => {
  const copy = R.clone(a)
  const parentId: number = a.parent
  const parent: any = parents.find( x => x.reference === parentId)
  if (!parent) throw Error(`tribe parent for ${a.id} not found`)
  copy.name = parent.name
  copy.effects = parent.effects
  copy.color = parent.color
  return copy
}

/**
   * Complète une carte objectif de tribu avec les infos de la carte parente
   * @param o objectif
   * @param parents parent objectives (all)
   * @returns carte complétée
   */
const completeObjective: (o: any, parents: any[]) => M.Objective = (o, parents) => {
  const copy = R.clone(o)
  const parentId: number = o.parent
  const parent: any = parents.find( x => x.reference === parentId)
  copy.name = parent.name
  copy.symbol = parent.symbol
  copy.steps = parent.steps
  copy.color = parent.color
  if (parent.special) copy.special = parent.special
  return copy
}

const completeTribeCard: (card: M.TribeCard) => M.TribeCard = card => M.isAction(card)
  ? completeAction(card, R.clone(parentActions))      // TODO: parentAction est changé si on le clone pas ? bug bizarre à vérifier
  : completeObjective(card, R.clone(parentObjectives))

const compileTribeCard: (tribeColors: {[color:string] : {[x: string]: string}}) => (card: M.Action | M.Objective) => M.Action | M.Objective =
    (tribeColors) => (card) => {
      if (M.isTribeCard(card)) {
        card = completeTribeCard(card)
      }
      card.color = `{{${card.color}}}`
      // TODO: pas très élégant, à réfléchir, le soucis c'est que sinon on remplace pas vu qu'on a pas les {{}} ...
      // il faudrait traduire les couleurs directement ...

      const colorDictionnary = tribeColors[getTribeOfCard(card)]
      if (M.isTribeCard(card) && M.isAction(card)) {
        const effects: any[] = []
        for (const effect of card.effects) {
          const price: M.Price = {}
          for (const lvl of ['major', 'medium', 'minor']) {
            if (effect.price[lvl]) {
              price[colorDictionnary[lvl]] = effect.price[lvl]
            }
          }
          effect.price = price
          effects.push(effect)
        }
        card.effects = effects
      }
      let color: string = card.color
      for (const lvl in colorDictionnary) {
        color = changePlaceHolder(lvl)(colorDictionnary[lvl])(color)
      }
      card.color = color
      let str:string = JSON.stringify(card)
      str = changeName(card.name)(str)
      if (M.isTribeCard(card)) {
        const colorDictionnary = tribeColors[getTribeOfCard(card)]
        str = changeRessources(colorDictionnary)(str)
      }
      return JSON.parse(str)
    }


export const loadTribeActionDeck: (deckName: string) => M.Action[] =
  (deckName: string) => parseFile(`${elements}tribus/${deckName}/deck_action.yml`)
    .map((x: M.Action) => {
      let id: string
      switch (deckName) {
        case 'green':
          id = 'm'
          break
        case 'blue':
          id = 'n'
          break
        case 'red':
          id = 'a'
          break
        default:
          id = ''
      }
      return R.mergeDeepRight(x, {set: 'tribe.' + id, type: 'tribe.action'})
    })

export const loadObjectiveDeck: (deckName: string) => M.Objective[] =
  (deckName: string) => parseFile(`${elements}tribus/${deckName}/deck_objective.yml`)
    .map((x: M.Objective) => {
      let id: string
      switch (deckName) {
        case 'green':
          id = 'm'
          break
        case 'blue':
          id = 'n'
          break
        case 'red':
          id = 'a'
          break
        default:
          id = ''
      }
      return R.mergeDeepRight(x, {set: 'tribe.' + id, type: 'objective'})
    })

// TODO: pour event et clone : faire generic
export const compileEventCard: (card: M.Event) => M.Event =
    (card) => {
      let str:string = JSON.stringify(card)
      str = changeName(card.name)(str)
      return JSON.parse(str)
    }

export const compileZoneCard: (card: M.Zone) => M.Zone =
    (card) => {
      let str:string = JSON.stringify(card)
      str = changeName(card.name)(str)
      return JSON.parse(str)
    }

export const compileCloneCard: (card: M.CloneCard) => M.CloneCard =
(card) => {
  let str:string = JSON.stringify(card)
  str = changeName(card.name)(str)
  return JSON.parse(str)
}

export const loadTribeDeck: (deckName: string) => Array<M.Action | M.Objective> =
    (deckName) => [
      ...loadTribeActionDeck(deckName),
      ...loadObjectiveDeck(deckName),
    ]
      .map(c => compileTribeCard(colorDictionnaries)(c))

export const loadEventDeck: () => M.Event[] =
    () => parseFile(`${elements}events.yml`)
      .map((x: M.Event) => {
        return R.mergeDeepRight(x, {type: 'event'})
      })
      .map(compileEventCard)

export const loadCloneActionDeck: (cloneName: string) => M.Action[] =
    (cloneName) => fs.existsSync(`${elements}clones/${cloneName}/actions.yml`) // metamorphe n'a pas d'actions ...
      ? parseFile(`${elements}clones/${cloneName}/actions.yml`)
        .map(compileCloneCard)
        .map((x: M.Action) => {
          return R.mergeDeepRight(x, {type: 'clone.action', set: 'clone.' + cloneName})
        })
      : []

export const loadMutationDeck: (cloneName: string) => M.Mutation[] =
    (cloneName) => parseFile(`${elements}clones/${cloneName}/mutations.yml`)
      .map(compileCloneCard)
      .map((x: M.Event) =>  R.mergeDeepRight(x, {type: 'mutation', set: 'clone.' + cloneName}))

export const loadCloneDeck: (cloneName: string) => Array<M.Action | M.Mutation> =
    (cloneName) => [...loadCloneActionDeck(cloneName), ...loadMutationDeck(cloneName)]
      .map(compileCloneCard)

export const loadCloneBoard: (cloneName: string) => M.CloneBoard =
  (cloneName) => R.mergeDeepRight(
    parseFile(`${elements}clones/${cloneName}/board.yml`) as M.CloneBoard,
    {cloneName: cloneName}
  )

export const loadZoneDeck: () => M.Zone[] =
    () => parseFile(`${elements}zones.yml`)
      .map((x: M.Zone) => R.mergeDeepRight(x, {type: 'zone'}))
      .map(compileZoneCard)

export const compile: (path: string) => (card: M.Card) => void =
    path => card => fs.writeFileSync(path + card.id + '.yml', stringify(card))

export const compileCloneBoard: (path: string) => (card: M.CloneBoard) => void =
  path => board => fs.writeFileSync(path + board.cloneName + '_board.yml', stringify(board))


// TODO: ajout des type pour events, clones, zones,
// et comment différencier action clone et action tribu ? faire  types différents qui se traitent pas mal pareil ? clone.action et tribe.action