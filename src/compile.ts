import { compile, compileCloneBoard, loadCloneDeck, loadEventDeck, loadTribeDeck, loadZoneDeck, loadCloneBoard } from './compilation/compilation'

// todo: config
export const cardsExportFolder: string = process.env.PWD + '/compiled/cards/'
export const boardsExportFolder: string = process.env.PWD + '/compiled/boards/'


// tribe cards
const deckColors = [
  'green',
  'blue',
  'red',
]
deckColors.map(color => loadTribeDeck(color))
  .map(d => d.map(c => compile(cardsExportFolder)(c)))

// events
loadEventDeck().map(compile(cardsExportFolder))

// clones
const cloneNames = [
  '13',
  'archeveque',
  'bestiau',
  'bulbe',
  'draineur',
  'homoncule',
  'infirmiere',
  'insectoide',
  'liquefactrice',
  'metamorphe',
  'nuage',
  'poche',
  'precog',
  'flagelleur',
  'trepane',
]

cloneNames.map(loadCloneDeck).map(d => d.map(compile(cardsExportFolder)))
cloneNames.map(loadCloneBoard).map(d => compileCloneBoard(boardsExportFolder)(d))


// zones
loadZoneDeck().map(compile(cardsExportFolder))