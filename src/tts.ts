// // TODO: retirer
// /* eslint-disable @typescript-eslint/no-explicit-any */

// import * as R from 'ramda'
// import * as S from 'sanctuary'
// //import * as ttsI from './interfaces/tts.interfaces'
// import { readFileSync } from 'fs'
// import {
//   cloneToPrintableMutations,
//   cloneToPrintableActions,
//   PrintableCard,
//   printableGreenDeck,
//   printableBlueDeck,
//   printableRedDeck,
//   printableEventDeck,
//   zoneDeck,
// } from './exportsFromYml'
// import { Clone, Color, Token, Tribe } from '../src/types/mutagene'
// import { clones, game } from './imports'  // TODO: il faudrait juste importe game, qui contient tout

// //import { config } from './load_config'

// //export const root = config.path.tts_root

// interface Position {
//   x: number,
//   y: number,
//   z: number
// }


// // configuration
// export const ttsPath: string = process.env.PWD + '/tts/'

// // position : inc 3 pas mal, marge à gauche encore si besoin

// // file loading
// const loadFile: (path: string) => (fileName: string) => any = (path) => (fileName) => JSON.parse(readFileSync(path + fileName, 'utf8'))

// export const loadFileInTTS: (fileName: string) => any = loadFile(ttsPath)

// // positionning
// const setObjectPosition: (object: any, position: Position | undefined) => any = (object, position = {x: 0, y: 0, z: 0}) =>
//   R.mergeRight(object, { 'Transform': R.mergeRight(object.Transform, {posX: position.x, posY: position.y, posZ: position.z}) })

// // deck manipulation
// const generateDeck: (guid: string) => ttsI.ObjectStateDeckI = guid => R.mergeRight(loadFileInTTS('empty_objects/empty_deck.json') as ttsI.ObjectStateDeckI, {GUID: guid})

// const addCardToDeck: (deck: ttsI.ObjectStateDeckI) => (card: PrintableCard) => ttsI.ObjectStateDeckI = deck => card => {
//   const newDeck = R.clone(deck)
//   const newCard = R.clone(card)
//   let cardCount: number = Object.keys(newDeck.CustomDeck).length
//   cardCount++
//   newDeck.CustomDeck[cardCount.toString()] = {
//     'FaceURL': root + newCard.face,
//     'BackURL': root + newCard.back,
//     'NumWidth': 1,
//     'NumHeight': 1,
//     'BackIsHidden': true,
//     'UniqueBack': false
//   }
//   // TODO: merge right sur une empty card
//   // ou même mieux : prendre en entrée pour addCardToDeck un ttsI.card tout propre sur lequel on merge right pour l'id
//   const completeCard: ttsI.ObjectStateCardI = {
//     'Name': 'CardCustom', // essential, used to identity the object type
//     'Transform': {
//       'posX': newDeck.Transform.posX,
//       'posY': newDeck.Transform.posY,
//       'posZ': newDeck.Transform.posZ,
//       'rotX': 0,
//       'rotY': 180,
//       'rotZ': 180,
//       'scaleX': 1.0,
//       'scaleY': 1.0,
//       'scaleZ': 1.0
//     },
//     'Nickname': newCard.name,
//     'Description': '',
//     'GMNotes': '',
//     'ColorDiffuse': {
//       'r': 0.713235259,
//       'g': 0.713235259,
//       'b': 0.713235259
//     },
//     'Locked': false,
//     'Grid': true,
//     'Snap': true,
//     'IgnoreFoW': false,
//     'Autoraise': true,
//     'Sticky': true,
//     'Tooltip': true,
//     'GridProjection': false,
//     'HideWhenFaceDown': true,
//     'Hands': true,
//     'CardID': cardCount * 100,
//     'SidewaysCard': false,
//     'CustomDeck': {},
//     'XmlUI': '',
//     'LuaScript': '',
//     'LuaScriptState': '',
//     'GUID': newCard.id
//   }

//   completeCard.CustomDeck[cardCount.toString()] = {
//     'FaceURL': root + newCard.face,
//     'BackURL': root + newCard.back,
//     'NumWidth': 1,
//     'NumHeight': 1,
//     'BackIsHidden': true,
//     'UniqueBack': false
//   }
//   newDeck.DeckIDs.push(cardCount * 100)
//   newDeck.ContainedObjects.push(completeCard)
//   return newDeck
// }

// const addCardsToDeck = (cards: PrintableCard[]) => (deck: ttsI.ObjectStateDeckI) => S.reduce(addCardToDeck)(deck)(cards)

// const createDeck: (deck: {guid: string, cards: PrintableCard[], position?: Position}) => ttsI.ObjectStateDeckI =
//   deck => addCardsToDeck(deck.cards)(setObjectPosition(generateDeck(deck.guid), deck.position) as ttsI.ObjectStateDeckI)

// // game manipulation
// const loadObjectToGame: (game: ttsI.GameI) => (object: ttsI.ObjectStateI) => ttsI.GameI = game => object => {
//   const newGame = R.clone(game)
//   newGame.ObjectStates.push(R.clone(object))
//   return newGame
// }

// const loadObjectsToGame: (game: ttsI.GameI) => (objects: ttsI.ObjectStateI[]) => ttsI.GameI = S.reduce(loadObjectToGame)

// const loadDeckToGame: (game: ttsI.GameI) => (deck: {guid: string, cards: PrintableCard[], position: Position}) => ttsI.GameI =
//   game => deck => loadObjectToGame(game)(createDeck(deck))

// const loadDecksToGame: (game: ttsI.GameI) => (decks: { guid: string, cards: PrintableCard[], position: Position }[]) => ttsI.GameI =
//   S.reduce(loadDeckToGame)


// // TODO: à terme, chaque clone sera un Bag avec cartes, pions et plaque

// // Plaques clones
// const emtyPlaque: ttsI.ObjectStateTileI = loadFileInTTS('empty_objects/empty_plaque.json')
// const clonesPositions: any [] = [
//   {x: 25, z: 15},
//   {x: 25, z: 10},
//   {x: 25, z: 5},
//   {x: 25, z: 0},
//   {x: 25, z: -5},
//   {x: 25, z: -10},
//   {x: 35, z: 15},
//   {x: 35, z: 10},
//   {x: 35, z: 5},
//   {x: 35, z: 0},
//   {x: 35, z: -5},
//   {x: 35, z: -10},
// ]
// const tribesPositions: Position[] = [
//   {x: -15, y: 0.8, z: -16},
//   {x: -12, y: 0.8, z: -16},
//   {x: -9, y: 0.8, z: -16},
// ]

// // const clonesPlaques: ttsI.ObjectStateTileI[] = clonesPositions.map(
// //   clone => setObjectPosition(
// //     R.mergeDeepRight(emtyPlaque, {
// //       CustomImage: {ImageURL: `${root}plaques/plaque_${clone.id}.jpg`},
// //       GUID: clone.id,
// //     }),
// //     {x: clone.x, y: 0.8218113,z: clone.z}
// //   ) as ttsI.ObjectStateTileI
// // )

// // pions clones
// export const emptyToken: ttsI.ObjectStateTileI = loadFileInTTS('empty_objects/empty_token.json')
// export const emptyTile: ttsI.ObjectStateTileI = loadFileInTTS('empty_objects/empty_tile.json')

// // Après : import des pions objectifs

// // RUN IT !

// export const decks: {guid: string, cards: PrintableCard[], position: Position}[] = [ // add clone with S.reduce(S.append)(array)(S.map(cloneDecks)(return ...))
//   {
//     guid: 'mutagene',
//     cards: printableGreenDeck,
//     position: tribesPositions[0],
//   },
//   {
//     guid: 'nutriment',
//     cards: printableBlueDeck,
//     position: tribesPositions[1],
//   },
//   {
//     guid: 'anticorps',
//     cards: printableRedDeck,
//     position: tribesPositions[2],
//   },
//   {
//     guid: 'events',
//     cards: printableEventDeck,
//     position: {x: -6, y: 0.8, z: -16}
//   },
//   {
//     guid: 'zones',
//     cards: zoneDeck,
//     position: {x: -3, y: 0.8, z: -16}
//   },
// ] // TODO map sur game.tribes, et faire tout ce qu'il faut

// // const tribeDecks = [
// //   {color: 'mutagene', cards: printableGreenDeck},
// //   {color: 'nutriment', cards: printableBlueDeck},
// //   {color: 'anticorps', cards: printableRedDeck},
// // ]


// const emptyBag: ttsI.ObjectStateBagI = loadFileInTTS('empty_objects/empty_bag.json')

// /**
//  * Add an object to the bag, accept null
//  * @param object to load
//  */
// const loadInBag: (object: ttsI.ObjectStateI | null) => (bag: ttsI.ObjectStateBagI) => ttsI.ObjectStateBagI =
//   (object) => (bag) => {
//     if (object === null) {
//       return bag
//     }
//     const newBag = R.clone(bag)

//     // newBag.ContainedObjects = newBag.ContainedObjects || [] // TODO: proble sur load file ?
//     newBag.ContainedObjects.push(object)

//     return newBag
//   }

// const makeToken: (token: {img: string, id: string} | string) => ttsI.ObjectStateTileI =
//   (token) => R.mergeDeepRight(
//     emptyToken,
//     {
//       CustomImage: {
//         ImageURL: `${root}pions/${(token as {img: string, id: string}).img || token}.png`,
//         ImageSecondaryURL: `${root}pions/${(token as {img: string, id: string}).img || token}.png`,
//       },
//       GUID: (token as {img: string, id: string}).id || token as string,
//     }
//   )


// const makeTile: (tile: {img: string, id: string} | string) => ttsI.ObjectStateTileI =
//   (tile) => R.mergeDeepRight(
//     emptyTile,
//     {
//       CustomImage: {
//         ImageURL: `${root}pions/${(tile as {img: string, id: string}).img || tile}.png`,
//         ImageSecondaryURL: `${root}pions/${(tile as {img: string, id: string}).img || tile}.png`,
//       },
//       GUID: (tile as {img: string, id: string}).id || tile as string,
//     }
//   )

// const emptyCuve: ttsI.ObjectStateTileI = loadFileInTTS('empty_objects/empty_cuve.json')

// const createCloneBag: (clone: Clone) => ttsI.ObjectStateBagI =
//   (clone) => {
//     let cloneBag: ttsI.ObjectStateBagI = R.clone(emptyBag)
//     cloneBag.ContainedObjects = cloneBag.ContainedObjects || [] // TODO: proble sur load file ?
//     cloneBag.Nickname = clone.name
//     cloneBag.GUID = clone.id + 'bag'
//     cloneBag.ColorDiffuse = {
//       r: clone.color === 'red' ? 1 : 0,
//       g: clone.color === 'green' ? 1 : 0,
//       b: clone.color === 'blue' ? 1 : 0,
//     }

//     // Tokens
//     cloneBag = (clone.tokens ?? []).reduce(
//       (b: ttsI.ObjectStateBagI, t: (string | {img: string, id: string})) => loadInBag(makeToken(t))(b),
//       cloneBag
//     )

//     // Tiles
//     cloneBag = (clone.tiles ?? []).reduce(
//       (b: ttsI.ObjectStateBagI, t: (string | {img: string, id: string})) => loadInBag(makeTile(t))(b),
//       cloneBag
//     )
//     // cuve
//     const cuve: ttsI.ObjectStateTileI | null = clone.cuve && clone.cuve.token ? R.mergeDeepRight(emptyCuve, {
//       CustomImage: {
//         ImageURL: `${root}pions/${(clone.cuve.token as {img: string, id: string}).img || clone.cuve.token}.png`,
//         ImageSecondaryURL: `${root}pions/${(clone.cuve.token as {img: string, id: string}).img || clone.cuve.token}.png`,
//       },
//       GUID: (clone.cuve.token as {img: string, id: string}).id || clone.cuve.token as string,
//     }) : null

//     // Plaque
//     const plaque: ttsI.ObjectStateTileI =  R.mergeDeepRight(emtyPlaque, {
//       CustomImage: {ImageURL: `${root}plaques/plaque_${clone.short}.jpg`},
//       GUID: clone.short + '_board',
//     })

//     // Actions
//     const actions: ttsI.ObjectStateDeckI | null = clone.actions
//       ? createDeck({
//         guid: clone.short + '_actions',
//         cards: cloneToPrintableActions(clone),
//       })
//       : null

//     // Mutations
//     const mutations: ttsI.ObjectStateDeckI | null = clone.mutations
//       ? createDeck({
//         guid: clone.short + '_mutations',
//         cards: cloneToPrintableMutations(clone),
//       })
//       : null

//     // Cubes PA / Blessure
//     const pa: ttsI.ObjectStateI = {
//       Name: 'BlockSquare',
//       Transform: {
//         posX: 0,
//         posY: 0,
//         posZ: 0,
//         rotX: 0,
//         rotY: 0,
//         rotZ: 0,
//         scaleX: 0.35,
//         scaleY: 0.35,
//         scaleZ: 0.35
//       },
//       Nickname: clone.short + '_pa',
//       Description: '',
//       GMNotes: '',
//       ColorDiffuse: {
//         r: .87,
//         g: .87,
//         b: .87
//       },
//       Locked: false,
//       Grid: false,
//       Snap: true,
//       IgnoreFoW: false,
//       Autoraise: true,
//       Sticky: true,
//       Tooltip: true,
//       GridProjection: false,
//       HideWhenFaceDown: false,
//       Hands: false,
//       XmlUI: '',
//       LuaScript: '',
//       LuaScriptState: '',
//       GUID: clone.short + '_pa'
//     }
//     const damage: ttsI.ObjectStateI = {
//       Name: 'BlockSquare',
//       Transform: {
//         posX: 0,
//         posY: 0,
//         posZ: 0,
//         rotX: 0,
//         rotY: 0,
//         rotZ: 0,
//         scaleX: 0.35,
//         scaleY: 0.35,
//         scaleZ: 0.35
//       },
//       Nickname: clone.short + '_damage',
//       Description: '',
//       GMNotes: '',
//       ColorDiffuse: {
//         r: 0.2,
//         g: 0.2,
//         b: 0.2,
//       },
//       Locked: false,
//       Grid: false,
//       Snap: true,
//       IgnoreFoW: false,
//       Autoraise: true,
//       Sticky: true,
//       Tooltip: true,
//       GridProjection: false,
//       HideWhenFaceDown: false,
//       Hands: false,
//       XmlUI: '',
//       LuaScript: '',
//       LuaScriptState: '',
//       GUID: clone.short + '_damage'
//     }

//     const cloneObjects: (ttsI.ObjectStateI | null)[] = [
//       pa,
//       cuve,
//       damage,actions,
//       mutations,
//       plaque,
//     ]

//     cloneBag = cloneObjects.reduce((b: ttsI.ObjectStateBagI, t: ttsI.ObjectStateI | null) => loadInBag(t)(b), cloneBag)

//     // cloneObjects.map(o => loadInBag(o)(cloneBag))
//     // cloneBag = cloneObjects.reduce((b, t) => loadInBag(t)(b), cloneBag)
//     return cloneBag
//   }

// const cloneBags: ttsI.ObjectStateBagI[] = clones.map(createCloneBag)

// const makeDice: (color: Color, guid: string) => ttsI.ObjectStateI =
//   (color, guid) => {
//     const props = {
//       GUID: guid,
//       ColorDiffuse: {
//         r: color === 'red' ? 0.5 : 0,
//         g: color === 'green' ? 0.5 : 0,
//         b: color === 'blue' ? 0.5 : 0,
//       }
//     }
//     return R.mergeRight(loadFileInTTS('empty_objects/empty_die.json') as ttsI.ObjectStateDeckI, props)
//   }

// const createTribeBag: (tribe: Tribe) => ttsI.ObjectStateBagI =
//   (tribe) => {
//     let tribeBag: ttsI.ObjectStateBagI = R.clone(emptyBag)
//     tribeBag.ContainedObjects = tribeBag.ContainedObjects || [] // TODO: proble sur load file ?
//     tribeBag.Nickname = tribe.name
//     tribeBag.GUID = tribe.color + 'bag'
//     tribeBag.ColorDiffuse = {
//       r: tribe.color === 'red' ? 1 : 0,
//       g: tribe.color === 'green' ? 1 : 0,
//       b: tribe.color === 'blue' ? 1 : 0,
//     }

//     // Tokens
//     tribeBag = (tribe.tokens ?? []).reduce(
//       (b: ttsI.ObjectStateBagI, t: Token) => loadInBag(makeToken(t))(b),
//       tribeBag
//     )

//     // Dices
//     const dices: ttsI.ObjectStateI[] = [1, 2, 3, 4, 5].map(x => makeDice(tribe.color, `${tribe.name}_dice_${x}`))

//     tribeBag = dices.reduce(
//       (b: ttsI.ObjectStateBagI, d: ttsI.ObjectStateI) => loadInBag(d)(b),
//       tribeBag
//     )

//     // Actions
//     //  const deck: ttsI.ObjectStateDeckI = createDeck({
//     //     guid: tribe.name + '_deck',
//     //     cards: tribeDecks.find(x => x.color === tribe.color)?.cards as PrintableCard[],
//     //     position: {x: 0, y: 0, z: 0}
//     //   })

//     //   tribeBag = loadInBag(deck)(tribeBag)

//     // TODO: pour l'instant simple mais il faudrait tout mettre
//     return tribeBag
//   }

// const tribeBags: ttsI.ObjectStateBagI[] = game.tribes.map(createTribeBag)

// // const table: ttsI.ObjectStateI[] = loadFileInTTS('table_test.json')
// const assets: ttsI.ObjectStateI[] = loadFileInTTS('elements_vrac.json')
// let ttsGame: ttsI.GameI =  loadObjectsToGame(loadFileInTTS('base_game.json'))([
//   // ...table,
//   ...assets,
//   ...cloneBags.map((bag, i) => setObjectPosition(bag, clonesPositions[i])),
//   ...tribeBags.map((bag, i) => setObjectPosition(bag, {x: tribesPositions[i].x, y: tribesPositions[i].y, z: tribesPositions[i].z + 3})),
// ])

// // TODO: récupérer les decks déjà formatés comme il faut

// ttsGame = loadDecksToGame(ttsGame)(decks)

// const pdf = loadFileInTTS('empty_objects/empty_rules.json')
// pdf.CustomPDF.PDFUrl = config.path.tts_root + '../mise_en_place.pdf'

// ttsGame = loadObjectToGame(ttsGame)(pdf)

// export { ttsGame as ttsGame }

// // base game : empty
// // clones :
// //    cartes
// //    tokens (cuve, clone, spéciaux)
// //    plaques
// // tribes :
// //    cartes (base)
// //    tokens (extracteurs, mains)
// // bactéries
// // flaunes
// // puits
// // boards (bag with tokens and tribe objectives)
// // events cards

// // méthode pour ajouter un élément à une position donnée
// // TODO: pile de X tokens de même type

// // bag de clone : récupérer les decks (mutations et actions à part), le token de base, la plaque, le pion cuve si y'a, pions spéciaux si y'a
// // toutes ces infos se récupèrent dans la description du clone (yml dans clones/*/clone.yml)