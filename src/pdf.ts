import markdownpdf from 'markdown-pdf'

// markdownpdf({paperFormat: 'A4'})
//   .from(process.env.PWD + '/regles/mise_en_place.md')
//   .to(process.env.PWD + '/mise_en_place.pdf', function () {
//     console.log('Done')
//   });



[
  '01-mise_en_place',
  '02-tour',
  '03-actions',
  '04-fin_de_partie',
  '05-mecanisme_generaux',
].map(name => markdownpdf({paperFormat: 'A4'})
  .from(process.env.PWD + '/regles/' + name + '.md')
  .to(process.env.PWD + '/pdf_exports/' + name + '.pdf', function () {
    console.log(name + ' done')
  }))