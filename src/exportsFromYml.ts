// // TODO: retirer eslint-disable

// import { parse } from 'yaml'
// import * as fs from 'fs'
// import * as R from 'ramda'

// import * as M from '../src/types/mutagene'
// //import { tribes, zones, events, clones } from './imports'

// export const elements: string = process.env.PWD + '/elements/'
// export const material: string = process.env.PWD + '/material/'
// const cards_exports_folder = 'cards_exports/'

// export interface PrintableCard {
//   name: string
//   id: string
//   face: string
//   back: string
//   guid: string
// }

// // prend un chemin complet et retourne le contenu
// const readFile: (path: string) => string = path => fs.readFileSync(path, 'utf8')

// export const parseFile: (path: string) => unknown = R.pipe(readFile, parse)


// // TODO: temporaire, le teps de tout migrer en image-manipulation

// const tribeToPrintableDeck = (tribe: M.Tribe) => [
//   ...tribe.actions ? tribe.actions.map((c: M.Action) => ({
//     name: c.name,
//     id: c.id,
//     face: `${cards_exports_folder}${c.id}_recto.jpg`,
//     back: `${cards_exports_folder}${c.id}_verso.jpg`,
//     guid: c.id
//   })) : [],
//   ...tribe.objectives ? tribe.objectives.map((c: M.Objective) => ({
//     name: c.name,
//     id: c.id,
//     face: `${cards_exports_folder}${c.id}_recto.jpg`,
//     back: `${cards_exports_folder}${c.id}_verso.jpg`,
//     guid: c.id
//   })) : [],
// ]

// export const cloneToPrintableMutations: (clone: M.Clone) => PrintableCard[] = (clone) => [
//   ...clone.mutations ? clone.mutations.map((c: M.Mutation) => ({
//     name: c.name,
//     id: c.id,
//     face: `${cards_exports_folder}${c.id}_recto.jpg`,
//     back: `${cards_exports_folder}${c.id}_verso.jpg`,
//     guid: c.id,
//   })) : [],
// ]

// export const cloneToPrintableActions: (clone: M.Clone) => PrintableCard[] = (clone) => [
//   ...clone.actions ? clone.actions.map((c: M.Action) => ({
//     name: c.name,
//     id: c.id,
//     face: `${cards_exports_folder}${c.id}_recto.jpg`,
//     back: `${cards_exports_folder}${c.id}_verso.jpg`,
//     guid: c.id
//   })) : [],
// ]

// export const printableGreenDeck: PrintableCard[] = tribeToPrintableDeck(tribes.find((x: { color: string }) => x.color === 'green') as M.Tribe)
// export const printableBlueDeck: PrintableCard[] = tribeToPrintableDeck(tribes.find((x: { color: string }) => x.color === 'blue') as M.Tribe)
// export const printableRedDeck: PrintableCard[] = tribeToPrintableDeck(tribes.find((x: { color: string }) => x.color === 'red') as M.Tribe)

// export const printableEventDeck: PrintableCard[] = events.map((c: { name: any; id: any }) => ({
//   name: c.name,
//   id: c.id,
//   face: `${cards_exports_folder}${c.id}_recto.jpg`,
//   back: `${cards_exports_folder}${c.id}_verso.jpg`,
//   guid: c.id
// }))

// export const zoneDeck: PrintableCard[] = zones.map((c: { name: any; id: any }) => ({
//   name: c.name,
//   id: c.id,
//   face: `${cards_exports_folder}${c.id}_HQ.jpg`,
//   back: `${cards_exports_folder}${c.id}_OP.jpg`,
//   guid: c.id
// }))

// // // TODO: use S.reduce
// //export const printableCloneDecks: PrintableCard[] = clones.map((c: M.Clone) => [
// //  ...cloneToPrintableMutations(c),
// //  ...cloneToPrintableActions(c),
// //]).reduce((acc: string | any[], val: any) => acc.concat(val), [])
