// // TODO: dégager tout ça

// /* eslint-disable @typescript-eslint/no-explicit-any */
// import { parse } from 'yaml'
// import * as fs from 'fs'
// import * as R from 'ramda'

// import * as M from '../src/types/mutagene'
// // import * as YML from './yml2mutagene'

// /**
//  * Export complet du jeu dans un json
//  * Permet entre autres de tester les diférences de version en git-diff
//  */

// const elements: string = process.env.PWD + '/elements/' // TODO: in config ?

// /**
//  * prend un chemin complet et retourne le contenu
//  * @param path chemin du fichier
//  * @returns
//  */
// const readFile: (path: string) => string = path => fs.readFileSync(path, 'utf8')

// const parseFile: (path: string) => any = R.pipe(readFile, parse)

// // events
// //export const events: M.Event[] = YML.loadEventDeck()

// // tribes
// export const ymlActions: any[] = parseFile(elements + 'tribus/actions.yml')
// export const ymlObjectives: any[] = parseFile(elements + 'tribus/objectives.yml')

// const tribeTokensFromFile: (tribeName: string) => M.Token[] = // TODO: ne pas tricher ?
//   tribeName => [
//     tribeName + '_extracteur_1',
//     ...[...Array(5)].map((x, i) => ({
//       img: tribeName + '_extracteur_2',
//       id: tribeName + '_extracteur_2_' + (i + 1),
//     })),
//     tribeName + '_extracteur_3',
//     {
//       id: tribeName + 'hand_4',
//       img: 'hand_4',
//     },
//     {
//       id: tribeName + 'hand_5',
//       img: 'hand_5',
//     },
//     {
//       id: tribeName + 'hand_6',
//       img: 'hand_6',
//     },
//   ]


// export const colorDictionnaries: {[color:string] : {[x: string]: string}} = {
//   'green': parseFile(`${elements}tribus/green/colors.yml`),
//   'blue': parseFile(`${elements}tribus/blue/colors.yml`),
//   'red': parseFile(`${elements}tribus/red/colors.yml`),
// }

// const tribeFromFile: (tribeName: M.Color) => M.Tribe = (tribeName: string) => {
//   if (!M.isColor(tribeName)) {
//     throw `${tribeName} is nor a Color`
//   }

//   return {
//     name: tribeName,
//     color: tribeName,
//     actions: YML.loadActionDeck(tribeName, colorDictionnaries),
//     objectives: YML.loadObjectiveDeck(tribeName, colorDictionnaries),
//     tokens: tribeTokensFromFile(tribeName)
//   }
//   // [...actionDeckFromFile(deckName), ...objectiveDeckFromFile(deckName)]
// }

// export const tribes: M.Tribe[] = [
//   tribeFromFile('green'),
//   tribeFromFile('blue'),
//   tribeFromFile('red'),
// ]

// // zones
// export const zones: M.Zone[] = YML.loadZoneDeck()


// // clones
// const cloneFromFile: (cloneName: string) => M.Clone = (cloneName) => {
//   const c = parseFile(`${elements}clones/${cloneName}/clone.yml`)
//   return {
//     name: c.name,
//     short: cloneName,
//     id: cloneName.substring(0, 3).toUpperCase(),
//     color: c.color,
//     mutations: YML.loadCloneMutationDeck(cloneName),
//     actions: YML.loadCloneActionDeck(cloneName),
//     tokens: c.tokens ?? [],
//     tiles: c.tiles ?? [],
//     cuve: c.cuve ?? null
//   }
//   // .reduce((prev: any, current: M.CloneCard) => {
//   //   prev[current.id] = current
//   //   return prev
//   // }, {})
// }

// export const clones: M.Clone[] = [
//   cloneFromFile('archeveque'),
//   cloneFromFile('bestiau'),
//   cloneFromFile('draineur'),
//   cloneFromFile('homoncule'),
//   cloneFromFile('infirmiere'),
//   cloneFromFile('insectoide'),
//   cloneFromFile('liquefactrice'),
//   cloneFromFile('metamorphe'),
//   cloneFromFile('nuage'),
//   cloneFromFile('poche'),
//   cloneFromFile('precog'),
//   cloneFromFile('trepane'),
// ]

// export const game: M.Game = {
//   tribes: tribes,
//   zones: zones,
//   events: [],
//   clones: clones,
//   boards: [],
//   tokens: [],
// }