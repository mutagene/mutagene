/**
 * Types for mutagene elements :
 *    - cards
 *    - clones
 *    - ...
 *
 * With testing for each of them
 * Necessary for all exports (DTP, TTS, ...)
 */
export type Color = 'green'|'blue'|'red'

// export type Price = {

//   [color in Color]?: number
// }
// TODO: obligé de se passer des couleurs à cause de la traduction major/ ... possible
export type Price = {[color:string] : number}

// TODO: complexifier les id pour reconnaitre id de deck tribu, clone, etc ...

export interface hasName {
    name: string
}

export type BaseCard = hasName & {
  id: string,
  set: string  // a specific tribe, clone, etc ... : 'tribe.a', 'clone.arc'
  type: string // tribe, clone, zone, etc
}

export type ActionEffect = {
  price: Price
  type: 'trash'|'burn'
  effect: string
}

export type Action = BaseCard & {
  color: Color|string // string lorsque pas traduit : TODO: confusion là un peu sur les différentes étapes d'une carte, peut être un peu trop de typage
  effects: ActionEffect[]
}

export type Special = 'mutation_slot' | 'double_mutation_slot' | 'objective_slot'

// TODO: distinguer spécial objective et special mutation et les lister
// TODO: permetre d'avoir plusieurs special sur une carte

export type Mutation = BaseCard & {
  color: Color
  base: string
  burn: string
  special?: Special[]
}

export type Objective = BaseCard & {
  color: Color
  symbol: string
  steps: string[]
  special?: Special[]
}

export type Headquarters = BaseCard & {
  hand: number
  wells: number
  genestock: number
  genotheque: {price: number, effect: string}[]
  passive?: string
}

export type Outpost = BaseCard & {
  wells: number
  effect: string
}

export type Zone = BaseCard & {
  symbol: string
  headquarters?: Headquarters
  outpost: Outpost
  outpost_2?: Outpost
}

export type SimpleEvent = BaseCard & {
  color: Color
  green: string
  blue: string
  red: string
}

export type GlobalEvent =  BaseCard & {color: Color, global: string}

export type Event = SimpleEvent | GlobalEvent

export type Card = Action | Objective | Mutation | Event | GlobalEvent | Zone

export type TribeCard = Action | Objective

export type Tribe = hasName & {
  color: Color
  actions: Action[]
  objectives: Objective[]
  tokens: Token[]
  // tokens: TODO: lister tokens ? genre en dur dans le type : extracteur_1 : 1, etc
}

export type CloneCard = Action | Mutation

export type CloneBoard = {
  cloneName: string
  effects: string[] // en vrai string + lettre + éventuellement position
  attributes: {[color:string] : number}
  slots: Special[] // ? à vérifier si ok, renommer à minima
  // TODO:
}

export type CloneCuve = {
  // TODO: number ? position, taille, etc
  token?: Token
}

// a simple token need just a string, different tokens with the same img need individual ids
export type Token = {img: string, id: string} | string

export type Clone = hasName & {
  short: string
  id: string
  color: Color
  actions?: Action[]
  mutations?: Mutation[]
  tokens?: Token[]
  tiles?: Token[]
  board?: CloneBoard,
  cuve?: CloneCuve
}

export type Board = hasName & {
}

export type Game = {
  clones: Clone[]
  events: (Event|GlobalEvent)[]
  tribes: Tribe[]
  boards: Board[]
  tokens: Token[]
  zones: Zone[] // TODO: caler ça dans les boards ? pour ranger propre ?
}



// type predicates

export const isColor = (color: unknown): color is Color => {
  if (color !== 'green' && color !== 'blue' && color !== 'red') {
    return false
  }
  return true
}

export const isPrice = (price: unknown): price is Price => {
  if (price === undefined) return false
  for (const color in price as Price) {
    if (!isColor(color)) return false
    if (typeof (price as never)[color] !== 'number' || (price as never)[color] < 0) {
      return false
    }
  }
  return true
}

export const isCard = (card: unknown): card is Card => {
  // TODO: détailler l'erreur ? (manque id, taille, etc ...)
  const c: BaseCard = card as BaseCard
  return (c.id !== undefined && typeof c.id === 'string' && c.id.length > 0) && (!!c.name && typeof c.name === 'string' && c.name.length > 0)
}

export const isActionEffect = (effect: unknown): effect is ActionEffect => {
  const e: ActionEffect = effect as ActionEffect
  if (!isPrice(e.price)) {
    return false
  }
  if (!e.type || typeof e.type !== 'string' || !(e.type === 'trash' || e.type === 'burn')) {
    return false
  }
  if (!e.effect || typeof e.effect !== 'string' || !e.effect.length) {
    return false
  }
  return true
}

export const isAction = (card: Card): card is Action => card.type.split('.')[1] === 'action'

export const isSpecial = (special: unknown): special is Special => {
  if (special !== 'mutation_slot' && special !== 'double_mutation_slot' && special !== 'objective_slot') {
    return false
  }
  return true
}

export const isMutation = (card: Card): card is Mutation => card.type === 'mutation'

// TODO: faire un isNotEmptyString ou autre ...

export const isObjective = (card: Card): card is Objective => card.type === 'objective'

export const isZone = (card: Card): card is Zone => card.type === 'zone'

export const isSimpleEvent = (event: unknown): event is Event => {
  if (!isColor((event as Event).color)) {
    return false
  }
  if (!isCard(event as Card)) {
    return false
  }
  // soit global soit couleurs
  const e: SimpleEvent = event as SimpleEvent
  return (
    (e.green !== undefined && typeof e.green === 'string' && e.green.length > 0)
    && (e.blue !== undefined && typeof e.blue === 'string' && e.blue.length > 0)
    && (e.red !== undefined && typeof e.red === 'string' && e.red.length > 0)
  )
}

export const isGlobalEvent = (event: unknown): event is GlobalEvent => {
  const e: GlobalEvent = event as GlobalEvent
  return (e.global !== undefined && typeof e.global === 'string' && e.global.length > 0)
}

export const isEvent = (card: Card): card is Event => card.type === 'event'

export const isCloneCard = (card: Card): card is CloneCard =>
  card.set !== undefined &&  card.set.split('.')[0] === 'clone'

export const isTribeCard = (card: Card): card is TribeCard =>
  card.set !== undefined && card.set.split('.')[0] === 'tribe'

export const isEventCard = (card: Card): card is Event =>
  card.set !== undefined && card.set.split('.')[0] === 'event'

export const isZoneCard = (card: Card): card is Zone =>
  card.set !== undefined && card.set.split('.')[0] === 'zone'