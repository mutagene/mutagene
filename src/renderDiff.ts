import { simpleGit } from 'simple-git'
import { renderCardFront, renderCardBack } from './rendering/renderCard'
import * as fs from 'fs'
import { parse } from 'yaml'
import { compilePicture } from 'image-manipulation'
import * as M from './types/mutagene'

// calcul du rendu seulement sur les cartes qui ont changé



// TODO: pour l'instant seulement les cartes, mais le reste va suivre
export const cardsExportFolder: string = process.env.PWD + '/compiled/cards/'



export const getRectoCardFileName: (card: M.Card) => string = (c: M.Card) => `${c.id}_${M.isZone(c) ? 'HQ' : 'recto'}.jpg`
export const getVersoCardFileName: (card: M.Card) => string = (c: M.Card) => `${c.id}_${M.isZone(c) ? 'OP' : 'verso'}.jpg`


async function main() {
  const diff = await simpleGit().diffSummary(cardsExportFolder)
  const files = diff.files.map(f => f.file)
  console.log(files)
  files
    .map(f => fs.readFileSync(f, 'utf8'))
    .map(x => parse(x))
    .filter(// TODO: à dégager
      c =>
        M.isAction(c)
          || M.isEvent(c)
          || M.isZone(c)
          || M.isMutation(c)
          || M.isObjective(c)
    )
    .map(
      async (card: M.Card) => {
        const path = process.env.PWD + '/material/cards_exports/'
        await compilePicture(renderCardFront(card), path + getRectoCardFileName(card))
        if (M.isZone(card)) {
          await compilePicture(renderCardBack(card), path + getVersoCardFileName(card))
        }
        // return await compilePicture(renderActionCardBack(card), path + getVersoCardFileName(card)) // TODO: zone
      }
    )

  // .map(c => compilePicture(renderActionCardFront(c), '')
  // à voir cette notion de PictureToCompile qui trimballe leur path : toujours pertinent ?
}

main()