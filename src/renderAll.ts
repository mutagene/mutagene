// va prendre toutes les cartes et faire le rendu

// TODO: il faudra le même script mais seulement sur les git-diff

// TODO: pour l'instant seulement les cartes, mais le reste va suivre

import { renderCardFront, renderCardBack } from './rendering/renderCard'
import * as fs from 'fs'
import { parse } from 'yaml'
import { compilePicture } from 'image-manipulation'
import * as M from './types/mutagene'

// calcul du rendu seulement sur les cartes qui ont changé



// TODO: pour l'instant seulement les cartes, mais le reste va suivre
export const cardsExportFolder: string = process.env.PWD + '/compiled/cards/'

// TODO: déplacer ça, on l'a en double dans renderDiff
export const getRectoCardFileName: (card: M.Card) => string = (c: M.Card) => `${c.id}_${M.isZone(c) ? 'HQ' : 'recto'}.jpg`
export const getVersoCardFileName: (card: M.Card) => string = (c: M.Card) => `${c.id}_${M.isZone(c) ? 'OP' : 'verso'}.jpg`


async function main() {
  fs.readdirSync(cardsExportFolder)
    .map(f => cardsExportFolder + f)
    .map(f => fs.readFileSync(f, 'utf8'))
    .map(x => parse(x))
  // .filter(// TODO: à dégager
  // c =>
  //     M.isAction(c)
  //     || M.isEvent(c)
  //     || M.isZone(c)
  //     || M.isMutation(c)
  //     || M.isObjective(c)
  // )
    .filter( // TODO: gérer A* par exemple
      c => process.argv.slice(2).length ? process.argv.slice(2).includes(c.id) : true
    )

  // todo : caler la position et taille des symboles mutation/obj sur celle du dos des cartes
    .map(
      async (card: M.Card) => {
        const path = process.env.PWD + '/material/cards_exports/'
        try {
          await compilePicture(renderCardFront(card), path + getRectoCardFileName(card)).then(() => console.log(path + getRectoCardFileName(card)))
        } catch (error) {
          console.log(`Error : ${error} [${card.id}]`)
          console.log(card)
          // throw error
        }
        if (M.isZone(card)) {
          await compilePicture(renderCardBack(card), path + getVersoCardFileName(card))
        }
      }
    )

  // .map(c => compilePicture(renderActionCardFront(c), '')
  // à voir cette notion de PictureToCompile qui trimballe leur path : toujours pertinent ?
}

main().catch(err => console.error(err))