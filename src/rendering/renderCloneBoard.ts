// /* eslint-disable */

// import * as M from '../types/mutagene'
// import { Picture, PositionedLayer, makePictureLayer, makeTextLayer } from 'image-manipulation/dist/src/image-manipulation'
// import { CARD_HEIGHT, CARD_WIDTH, TITLE_FONT_SIZE } from './const'

// import { execSync } from 'child_process'

// const ID_FONT = 'Agent \'C\''
// const TEXT_FONT = 'Open Dyslexic'
// const TEXT_FONT_SIZE = 20
// const BORDER_SIZE = 10

// const TITLE_FONT = 'Fundead RR'

// /**
//  * Suppression des accents et upperCase
//  * @param title
//  */
// const titlelize = (title: string) => title.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toUpperCase()

// const getGitVersion = (): string => execSync('git describe --tags --abbrev=0').toString().trim()

// export const generateVersionLayer = (): PositionedLayer =>
//   makeTextLayer({x: 660, y: 1000, fontSize: 32, font: ID_FONT, text: getGitVersion()})


// export const generateIdLayer = (id: string): PositionedLayer =>
//   makeTextLayer({x: 20, y: 1000, fontSize: 32, font: ID_FONT, text: id})


// const generateBoardTitleLayer = (title: string) => makeTextLayer({
//   x: title.length > 17 ? 180 : 195,
//   y: title.length > 17 ? 32 : 36,
//   fontSize: TITLE_FONT_SIZE,
//   font: TITLE_FONT,
//   text: titlelize(title),
//   width: title.length > 17 ? 520 : 480,
//   height: 120,
//   center: true
// })


// export const renderCloneBoard: (board: M.CloneBoard) => Picture =
// board => {
//       const title = `${process.env.PWD}/pao/${board.color}_title.png`

//       const effect1: PositionedLayer = makeTextLayer({
//         x: 205,
//         y: 570,
//         fontSize: TEXT_FONT_SIZE,
//         font: TEXT_FONT,
//         text: board.effects[0].effect,
//         width: 525
//       })
//       const effect2: PositionedLayer = makeTextLayer({
//         x: 205,
//         y: 817,
//         fontSize: TEXT_FONT_SIZE,
//         font: TEXT_FONT,
//         text: board.effects[1].effect,
//         width: 525
//       })

//       const effect1symbol: PositionedLayer = makePictureLayer(
//         45,
//         620,
//         process.env.PWD + `/pao/symboles/picto_${board.effects[0].type}.png`,
//         {ratio: .3}
//       )
//       const effect2symbol: PositionedLayer = makePictureLayer(
//         45,
//         830,
//         process.env.PWD + `/pao/symboles/picto_${board.effects[1].type}.png`,
//         {ratio: .3}
//       )
//       const picture: Picture = {
//         width: CARD_WIDTH,
//         height: CARD_HEIGHT,
//         border: {size: BORDER_SIZE},
//         layers: [
//           // BACKGROUND
//           makePictureLayer(0, 0, ACTION_BACKGROUND),
//           makePictureLayer(0, 0, title),
//           // ID
//           generateIdLayer(card.id),
//           // Version
//           generateVersionLayer(),
//           // NAME
//           generateTitleLayer(card.name),
//           // EFFECTS
//           effect1symbol,
//           effect1,
//           effect2symbol,
//           effect2,
//           ...generatePriceLayers(card.effects[0], 570),
//           ...generatePriceLayers(card.effects[1], 815),
//         ]
//       }
//       return picture
//     }


