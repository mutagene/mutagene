
import { execSync } from 'child_process'


/**
 * Suppression des accents et upperCase
 * @param title
 */
export const titlelize = (title: string) => title.normalize('NFD').replace(/[\u0300-\u036f]/g, '') // .toUpperCase()


export const getGitVersion = (): string => execSync('git describe --tags --abbrev=0').toString().trim()