import * as R from 'ramda'
import * as M from '../../types/mutagene'
import { Picture, PositionedLayer, makeCircleLayer, makePictureLayer, makeTextLayer } from 'image-manipulation/dist/src/image-manipulation'
import { CARD_HEIGHT, CARD_WIDTH, BORDER_SIZE, ID_FONT, TEXT_FONT, TEXT_FONT_SIZE } from '../const'
import { generateIdLayer, generateVersionLayer, generateTitleLayer } from './renderCardsCommon'


const ACTION_BACKGROUND: string = process.env.PWD + '/pao/cards_front/action_background.jpg'

const generateTitleBorder: (color: string) => PositionedLayer[] =
    color => {
      return [ makePictureLayer(
        0,
        0,
        process.env.PWD + `/pao/${color}_title_bar.png`
      )]
    }

export const renderActionCardFront: (card: M.Action) => Picture =
    card => {
      if (!M.isAction(card)) throw Error(`${(card as any).id} is not an action`)

      // const title = `${process.env.PWD}/pao/${card.color}_title.png`

      const effect1: PositionedLayer = makeTextLayer({
        x: 205,
        y: 570,
        fontSize: TEXT_FONT_SIZE,
        font: TEXT_FONT,
        text: card.effects[0].effect,
        width: 525,
        weight: 'bold'
      })
      const effect2: PositionedLayer = makeTextLayer({
        x: 205,
        y: 817,
        fontSize: TEXT_FONT_SIZE,
        font: TEXT_FONT,
        text: card.effects[1].effect,
        width: 525,
        weight: 'bold'
      })

      const effect1symbol: PositionedLayer = makePictureLayer(
        45,
        620,
        process.env.PWD + `/pao/symboles/picto_${card.effects[0].type}.png`,
        {ratio: .3}
      )
      const effect2symbol: PositionedLayer = makePictureLayer(
        45,
        830,
        process.env.PWD + `/pao/symboles/picto_${card.effects[1].type}.png`,
        {ratio: .3}
      )
      const picture: Picture = {
        width: CARD_WIDTH,
        height: CARD_HEIGHT,
        border: {size: BORDER_SIZE},
        layers: [
          // BACKGROUND
          makePictureLayer(0, 0, ACTION_BACKGROUND),
          // makePictureLayer(0, 0, title),
          // ID
          generateIdLayer(card.id),
          // Version
          generateVersionLayer(),
          // NAME
          ...generateTitleBorder(card.color),
          generateTitleLayer(card.name),
          // EFFECTS
          effect1symbol,
          effect1,
          effect2symbol,
          effect2,
          ...generatePriceLayers(card.effects[0], 570),
          ...generatePriceLayers(card.effects[1], 815),
        ]
      }
      return picture
    }




export const renderActionCardBack: (card: M.Action) => Picture =
card => {
  if (!M.isAction(card)) throw Error(`${(card as any).id} is not an action`)

  const genericId: string = card.set.split('.')[1]
  const isClone = false // TODO: à faire, ouais

  return {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    border: {size: BORDER_SIZE},
    layers: [
      makePictureLayer(0, 0, `${process.env.PWD}/pao/cards_back/${genericId}_${card.color}_verso.jpg`),
      makeTextLayer({x: 20, y: 1000, fontSize: 32, font: ID_FONT, text: card.id.substring(0, isClone ? 3 : 1)}),
    ]
  }
}


const generatePriceLayers = (effect: M.ActionEffect, top: number): PositionedLayer[] => {
  const priceLayers: PositionedLayer[] = []
  let i = 0
  for (const color in effect.price) { // TODO: parcourir dans l'ordre
    R.range(0, effect.price[color as M.Color] as number).forEach(() => {
      let rgb
      switch (color) {
        case 'green':
          rgb = '#1E9B1A'
          break
        case 'blue':
          rgb = '#0694FF'
          break
        case 'red':
          rgb = '#FC1414'
          break
        default:
          throw Error('invlid color')
      }
      const layer = makeCircleLayer(160, top + i * 38, 18, rgb)
      priceLayers.push(layer)
      i++
    })
  }
  return priceLayers
}
  
  