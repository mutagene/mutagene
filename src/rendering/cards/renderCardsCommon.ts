
import { PositionedLayer, makeTextLayer } from 'image-manipulation/dist/src/image-manipulation'
import { titlelize } from '../utils'
import { execSync } from 'child_process'
import {
  ID_FONT,
  TITLE_FONT_SIZE,
  TITLE_FONT,
  TITLE_HEIGHT,
  TITLE_WIDTH,
  TITLE_X,
  TITLE_Y,
} from '../const'

const getGitVersion = (): string => execSync('git describe --tags --abbrev=0').toString().trim()

export const generateIdLayer = (id: string): PositionedLayer =>
  makeTextLayer({x: 20, y: 985, fontSize: 30, font: ID_FONT, text: id})

export const generateVersionLayer = (): PositionedLayer =>
  makeTextLayer({x: 660, y: 985, fontSize: 30, font: ID_FONT, text: getGitVersion()})

export const generateTitleLayer = (title: string) => makeTextLayer({
  x: TITLE_X,
  y: TITLE_Y,
  fontSize: TITLE_FONT_SIZE,
  font: TITLE_FONT,
  text: titlelize(title),
  width: TITLE_WIDTH,
  height: TITLE_HEIGHT,
  center: true
})
  