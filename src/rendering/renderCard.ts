import * as M from '../types/mutagene'
import { Picture, PositionedLayer, makePictureLayer, makeTextLayer } from 'image-manipulation/dist/src/image-manipulation'
import { renderActionCardFront, renderActionCardBack } from './cards/renderCardsAction'
import { generateIdLayer, generateVersionLayer, generateTitleLayer } from './cards/renderCardsCommon'
import { titlelize } from './utils'

import {
  CARD_HEIGHT,
  CARD_WIDTH,
  BORDER_SIZE,
  TEXT_FONT,
  TEXT_FONT_SIZE,
  TITLE_FONT_SIZE,
  TITLE_FONT,
  TITLE_HEIGHT,
  TITLE_WIDTH,
  TITLE_X,
  TITLE_Y,
} from './const'


const MUT_BACKGROUND: string = process.env.PWD + '/pao/cards_front/mut_background.jpg'

const OBJ_BACKGROUND: string = process.env.PWD + '/pao/cards_front/obj_background.jpg'

const EVENT_BACKGROUND: string = process.env.PWD + '/pao/cards_front/event_background.jpg'
const EVENT_GLOBAL_BACKGROUND: string = process.env.PWD + '/pao/cards_front/event_global_background.jpg'


export const renderCardFront: (card: M.Card) => Picture =
    card => {
      if (M.isAction(card)) {
        return renderActionCardFront(card)
      }
      if (M.isEvent(card)) {
        return renderEventCardFront(card)
      }
      if (M.isZone(card)) {
        return renderZoneCardFront(card)
      }
      if (M.isMutation(card)) {
        return renderMutationCardFront(card)
      }
      if (M.isObjective(card)) {
        return renderObjectiveCardFront(card)
      }
      throw `undefined card type : ${(card as M.Card).type ?? 'UNDEFINED'}`
    }

export const renderCardBack: (card: M.Card) => Picture =
    card => {
      if (M.isAction(card)) {
        return renderActionCardBack(card)
      }
      // if (M.isEvent(card)) {
      //     return renderEventCardBack(card)
      // }
      if (M.isZone(card)) {
        return renderZoneCardBack(card)
      }
      // if (M.isMutation(card)) {
      //     return renderMutationCardBack(card)
      // }=
      // if (M.isObjctive(card)) {
      //     return renderObjectiveCardBack(card)
      // }
      throw `undefined card type : ${card.type}`
    }



const makeSpecialLayers: (card: M.Objective | M.Mutation) => PositionedLayer[] =
    // TODO: y'a du travail là : est ce que les specials sont cumulables ?
    // si non il faut vérifier, si oui il faut leur fixer des positions ...
    // bref, à voir tout ça
    (card) => (card.special || []).map(special =>
      makePictureLayer(
        special === 'double_mutation_slot' ? 300 : 355,
        950,
        `${process.env.PWD}/pao/symboles/${special}.png`,
        {ratio: .5}
      )
    )

const renderMutationCardFront: (card: M.Mutation) => Picture =
card => {
  if (!M.isMutation(card)) throw Error(`${(card as any).id} is not a mutation`)

  const title = `${process.env.PWD}/pao/${card.color}_title.png`

  const base: PositionedLayer = makeTextLayer({
    x: 160,
    y: 570,
    fontSize: TEXT_FONT_SIZE,
    font: TEXT_FONT,
    text: card.base,
    width: 570,
    weight: 'bold'
  })

  const burn: PositionedLayer = makeTextLayer({
    x: 160,
    y: 810,
    fontSize: TEXT_FONT_SIZE,
    font: TEXT_FONT,
    text: card.burn,
    width: 570,
    weight: 'bold'
  })

  const picture: Picture = {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    border: {size: BORDER_SIZE},
    layers: [
      makePictureLayer(0, 0, MUT_BACKGROUND),
      makePictureLayer(0, 0, title),
      // TODO: à affiner, à mettre en const et à utiliser pour le dos
      // mais il faut mettre les visuels à jour niveau taille d'abord, mutation et obj ont pas la même taille

      // ici utiliser constante qu'on reprendra pour le verso
      makePictureLayer(CARD_WIDTH - 20 - 150, 20, `${process.env.PWD}/pao/symboles/mutation.png`, {ratio: .35}),
      generateIdLayer(card.id),
      generateVersionLayer(),
      generateTitleLayer(card.name),
      base,
      burn,
      ...makeSpecialLayers(card),
    ]
  }
  return picture
}

const generateEventTitleLayer = (title: string) => makeTextLayer({
  x: TITLE_X,
  y: TITLE_Y,
  fontSize: TITLE_FONT_SIZE,
  font:  TITLE_FONT,
  text: titlelize(title),
  width: TITLE_WIDTH,
  height: TITLE_HEIGHT,
  center: true,
  color: 'black'
})

const createSimpleEventFront: (card: M.SimpleEvent) => Picture = (card) => {
  if (!M.isSimpleEvent(card)) throw Error(`${(card as any).id} is not an event`)

  const flaune = '<b>Posez un pion Flaune.</b>\\n'
  const green: string = (card.color === 'green' ? flaune : '') + card.green
  const blue: string = (card.color === 'blue' ? flaune : '') + card.blue
  const red: string = (card.color === 'red' ? flaune : '') + card.red

  const greenLayer: PositionedLayer = makeTextLayer({
    x: 50,
    y: 255,
    fontSize: TEXT_FONT_SIZE,
    font: TEXT_FONT,
    text: green,
    width: 640,
    color: card.color === 'green' ? '"#1e9b1a"' : 'black',
    weight: 'bold'
  })

  const blueLayer: PositionedLayer = makeTextLayer({
    x: 50,
    y: 525,
    fontSize: TEXT_FONT_SIZE,
    font: TEXT_FONT,
    text: blue,
    width: 640,
    color: card.color === 'blue' ? '"#0694e1"' : 'black',
    weight: 'bold'
  })

  const redLayer: PositionedLayer = makeTextLayer({
    x: 50,
    y: 800,
    fontSize: TEXT_FONT_SIZE,
    font: TEXT_FONT,
    text: red,
    width: 640,
    color: card.color === 'red' ? '"#fc1414"' : 'black',
    weight: 'bold'
  })

  return {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    border: {size: BORDER_SIZE},
    layers: [
      // BACKGROUND
      makePictureLayer(0, 0, EVENT_BACKGROUND),
      // ID
      generateIdLayer(card.id),
      // Version
      generateVersionLayer(),
      // NAME
      generateEventTitleLayer(card.name),

      greenLayer,
      blueLayer,
      redLayer,
    ]
  }
}

const createGlobalEventFront: (card: M.GlobalEvent) => Picture = (card) => {
  if (!M.isGlobalEvent(card)) throw Error(`${(card as any).id} is not a global event`)

  let rgb
  switch (card.color) {
    case 'green':
      rgb = '"#1E9B1A"'
      break
    case 'blue':
      rgb = '"#0694FF"'
      break
    case 'red':
      rgb = '"#FC1414"'
      break
    default:
      throw Error('invlid color')
  }

  // const title = `${process.env.PWD}/pao/${card.color}_title.png`

  const effectLayer: PositionedLayer = makeTextLayer({
    x: 50,
    y: 250,
    fontSize: TEXT_FONT_SIZE,
    font: TEXT_FONT,
    text: card.global,
    width: 640,
    weight: 'bold'
  })

  const flauneLayer: PositionedLayer = makeTextLayer({
    x: 50,
    y: 580,
    fontSize: TEXT_FONT_SIZE,
    font: TEXT_FONT,
    text: '<b>Posez un pion Flaune.</b>',
    width: 640,
    color: rgb,
    weight: 'bold'
  })

  return {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    border: {size: BORDER_SIZE},
    layers: [
      // BACKGROUND
      makePictureLayer(0, 0, EVENT_GLOBAL_BACKGROUND),
      // makePictureLayer(0, 0, title),
      // ID
      generateIdLayer(card.id),
      // Version
      generateVersionLayer(),
      // NAME
      generateEventTitleLayer(card.name),
      flauneLayer,
      effectLayer,
    ]
  }
}

export const renderEventCardFront: (card: M.Event) => Picture =
    card => {
      if (!M.isEvent(card)) throw Error(`${(card as any).id} is not an event`)

      return M.isGlobalEvent(card)
        ? createGlobalEventFront(card)
        : createSimpleEventFront(card)
    }


export const renderZoneCardFront: (card: M.Zone) => Picture =
card => {
  if (!M.isZone(card)) throw Error(`${(card as any).id} is not a zone`)
  if (card.outpost_2) {
    let card2: M.Zone = JSON.parse(JSON.stringify(card));
    card2.outpost = card.outpost_2
    return renderZoneCardBack(card2)
  }
  if (!card.headquarters) {
    throw 'no headquarters'
  }


  const genothequeConcat: string = card.headquarters.genotheque.map(g => `${g.price} carte${g.price > 1 ? 's' : ''} génothèque : ${g.effect}`).join('\n\n')
    + (card.headquarters.passive ? '\n\n' + card.headquarters.passive : '')
  return {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    border: {size: BORDER_SIZE},
    layers: [

      makePictureLayer(0, 0, `${process.env.PWD}/pao/cards_front/hq_background.jpg`),
      // ID
      generateIdLayer(card.id),
      // Version
      generateVersionLayer(),
      // NAME
      generateTitleLayer(card.name),

      // Obj Symbol
      makePictureLayer(13, 10, `${process.env.PWD}/pao/objectives/${card.symbol}.png`),

      // Hand value
      makeTextLayer({
        x: 150,
        y: 200,
        fontSize: 120,
        font: TITLE_FONT,
        text: card.headquarters.hand.toString(),
        height: 150,
      }),

      // Well value
      makeTextLayer({
        x: 150,
        y: 370,
        fontSize: 120,
        font: TITLE_FONT,
        text: card.headquarters.wells.toString(),
        height: 150,
      }),

      // Genotek value
      makeTextLayer({
        x: 55,
        y: 740,
        fontSize: 80,
        font: TITLE_FONT,
        text: card.headquarters.genestock.toString(),
        height: 150,
      }),

      // Genotek Symbol
      makePictureLayer(40, 600, `${process.env.PWD}/pao/symboles/geno_hq.png`, {ratio: 0.85}),

      // Genotek fleche
      makePictureLayer(47, 820, `${process.env.PWD}/pao/symboles/fleche_hq.png`, {ratio: 0.9}),

      makeTextLayer({
        x: 200,
        y: 600,
        fontSize: TEXT_FONT_SIZE,
        font: TEXT_FONT,
        text: genothequeConcat,
        width: 500,
        weight: 'bold'
      })
    ]
  }
}

export const renderZoneCardBack: (card: M.Zone) => Picture =
card => {
  if (!M.isZone(card)) throw Error(`${(card as any).id} is not a zone`)

  return {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    border: {size: BORDER_SIZE},
    layers: [

      makePictureLayer(0, 0, `${process.env.PWD}/pao/cards_front/op_background.jpg`),
      // ID
      generateIdLayer(card.id),
      // Version
      generateVersionLayer(),
      // NAME
      generateTitleLayer(card.name),

      // Obj Symbol
      makePictureLayer(13, 10, `${process.env.PWD}/pao/objectives/${card.symbol}.png`),


      // Obj Symbol
      // makePictureLayer(13, 10, `${process.env.PWD}/pao/objectives/${card.symbol}.png`),

      // Well value
      makeTextLayer({
        x: 125,
        y: 160,
        fontSize: 120,
        font: TITLE_FONT,
        text: card.outpost.wells.toString(),
        height: 150,
      }),

      // EFFECT
      makeTextLayer({
        x: 50,
        y: 600,
        fontSize: TEXT_FONT_SIZE,
        font: TEXT_FONT,
        text: card.outpost.effect,
        width: 650,
      })
    ]
  }
}


export const renderObjectiveCardFront: (card: M.Objective) => Picture =
card => {
  if (!M.isObjective(card)) throw Error(`${(card as any).id} is not an objective`)
  const title = `${process.env.PWD}/pao/${card.color}_title.png`
  const symbol = `${process.env.PWD}/pao/objectives/${card.symbol}.png`
  return {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    border: {size: BORDER_SIZE},
    layers: [

      // BACKGROUND
      makePictureLayer(0, 0, OBJ_BACKGROUND),
      makePictureLayer(0, 0, title),
      // ID
      generateIdLayer(card.id),
      // Version
      generateVersionLayer(),
      // NAME
      generateTitleLayer(card.name),
      // SYMBOL
      makePictureLayer(CARD_WIDTH - 160, 10, symbol),
      // STEPS
      makeTextLayer({
        x: 55,
        y: 575,
        fontSize: TEXT_FONT_SIZE,
        font: TEXT_FONT,
        text: card.steps.join('\\n'),
        width: 670,
        weight: 'bold'
      }),
      // SPECIALS
      ...makeSpecialLayers(card),
    ]
  }
}