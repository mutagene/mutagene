#!/bin/bash

size_from="635x887"
size_to="635x881+0-1"

# TODO: add args for in and out folders
for file in ./material/cards.verso.save/*.jpg; do
  out=$(basename $file)
  convert $file \
    -resize $size_from \
    -crop $size_to \
    /home/jinfante/projects/mutagene/material/cards_exports/$out
done

# gray -> color
#convert archeveque_back.jpg -fill "#1E9B1A" -tint 40 archeveque_back_green.jpg
#convert archeveque_back.jpg -fill "#0694FF" -tint 40 archeveque_back_blue.jpg
#convert archeveque_back.jpg -fill "#FC1414" -tint 40 archeveque_back_red.jpg