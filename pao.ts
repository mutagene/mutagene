export const pao = {

  config: {}, // misc (paths, etc)

  layouts: {  // parties de pao communs qui pourront être réutilisés
    title: {
      type: 'text',
      pos: {x: 12, y: 12}
      // ...
    },
    action_card: {
      type: 'composition',  // c'est pas uin calque mais une construction qu'on pourra surcharger

    }
  },

  elements: {
    clones: {
      actions_cards: {
        'inherit': 'layouts.action_card'
      }
    },
    tribes: {

    }
  }
}