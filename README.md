# mutagene

Règles et matériel pour mutagene, le jeu de plateau mutant.

Rules and material for mutagene, the mutant boardgame.

## print

https://gitlab.com/api/v4/projects/13656115/jobs/artifacts/master/raw/print/print.zip?job=print

## Présentation du jeu

Mutagene est un jeu de plateau pour 2 à 3 joueurs (un mode 4 joueurs est dans les tuyaux), d'une durée de 2/3 heures. Les joueurs (*tribus*) s'affronter des équipes de 3 personnages (*clones*) sur un champs de bataille d'un univers post-apocalyptique. À l'aide de capacités spéciales (*mutations* et *cartes d'action*) ils doivent accomplir des *objectifs* et se développer pendant que l'environnement hostile (la *flaune*) envahit progressivement la surface de jeu.

Chaque *clone* dispose de points d'action (*PA*) qui leur permettent de réaliser des actions génériques, et les *blessures* qu'ils subissent réduisent leur capacité d'action. La mort d'un *clone* n'est pas définitive, et les *mutations* qu'ils possèdent peuvent changer en cours de partie.

Les tribus jouent une pioche cartes composées des cartes propres aux 3 *clones* choisis, ainsi que des cartes actions et objectifs (en l'état identitiques d'une tribus à l'autre à l'exception de leur prix). Elle payent leurs cartes à l'aide de ressources gagnées en posant des *extracteurs* sur des *puits* présents sur le plateau. Ils peuvent également utiliser des cartes pour en payer d'autres, réduisant cependant leurs possibilités pour le tour.

Un tour de jeu se joue un clone après l'autre en alternant les tribus. Chaque clone joue ainsi une séquence où il va dépenser ses PA pour se déplacer, réaliser ses objectifs, soigner ses alliés, attaquer ses adversaires, etc ... Il peut en conserver pour une autre séquence, et une fois que tout les clones ont joués un nouveau tour commence où chacun regagne ses PA et des ressources.

La main de chaque tribu est renouvellée au début de chaque séquence, et lorsqu'une tribu n'a plus de cartes dans sa pioche, elle mélange sa défausse pour en constituer une nouvelle. Les cartes peuvent être jouées normalement en étant défaussée, ou de manière "augmentée" ou n'allant pas dans la défausse mais dans le *charnier* d'où elle ne sortiront plus (à quelques exceptions près ...), réduisant ainsi la taille de la pioche au fur et à mesure que la partie avance.

Certaines cases indiquent des objectifs à travers des symboles spécifiques qui se retrouvent dans les cartes objectifs de chaque tribu. Chaque carte objectif possèdent des conditions spéciales pour être réalisées et rapporter des points de victoires (à commencer par se rendre sur la case appropriée).

Un clone ne pouvant avoir qu'un seul objectif en cours de réalisation à la fois (et ce dernier lui occupe un emplacement pouvant également accueillir une mutation supplémentaire), chaque joueur doit répartir ses clones de manière appropriés sur le terrain, en essayant au passage de ralentir les adversaires, d'éviter ces derniers de les blesser, placer des extracteurs de ressources supplémentaires, soigner ses camarades, éviter la flaune, ...

## Projet

Le développement de ce jeu, de son univers et de ses outils n'est pas sujet à une sortie claire sous une forme ou une autre (publication, print'n'play, crowfunding, etc). Son objet premier est de se croître, de s'adapter, d'évoluer. Toute étape intermédiaire (le jeu est à sa troisième version majeure) a atteint son objectif : faire jouer.

Plus de visibilité aujourd'hui est rendue possible avec l'utilisation d'outils pour accompagner le développement du projet (Gitlab en tête). Le mod Tabletop Simulator vise également à faciliter le partage et la diffusion de Mutagene.

## Langage

Ce projet est en français en attendant un travail de traduction.

This project is in French while awaiting translation work.


## Objet des outils

Ce projet contient égalements les outils pour automatiser l'export des assets graphiques ainsi que de parties sur Tabletop Simulator.


### Requirements

ts-node for direct execution ```ts-node ./src/*.ts```
imagemagick for image manipulation

## Tabletop Simulator

Un mod TTS est en cours de développement. Il est tenu à jour avec la dernière version stable du matériel du jeu. Des scripts sont prévus afin de faciliter la mise en place des parties.

![tts working mod](/visuels/mutamod_tts_screen.png)

Un Workshop public sera mis à disposition dès le jeu stabilisé, ainsi qu'un premier jet de règles permettant de tester. Pour des explications, démo, etc, contactez nous via Discord.


### Linux

~/.local/share/Tabletop Simulator/Mods/Workshop

~/.local/share/Tabletop Simulator/Saves

### Mac

~/Bibliothèque/Tabletop Simulator

## Social

[Discord](https://discord.gg/JkZPryX)
