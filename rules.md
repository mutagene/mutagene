# MUTAGENE 3.0 RULES

## Matériel

* Plateaux (6)

<!-- <img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plateaux/plateau_1.png" width="200" height="200" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plateaux/plateau_2.png" width="200" height="200" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plateaux/plateau_3.png" width="200" height="200" />

<br/>

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plateaux/plateau_4.png" width="200" height="200" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plateaux/plateau_5.png" width="200" height="200" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plateaux/plateau_6.png" width="200" height="200" /> -->

* Plaques de clones (12)

<!-- <img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_archeveque.jpg" width="300" height="100" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_bestiau.jpg" width="300" height="100" />

<br/>

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_draineur.jpg" width="300" height="100" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_homoncule.jpg" width="300" height="100" />

<br/>

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_insectoide.jpg" width="300" height="100" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_liquefactrice.jpg" width="300" height="100" />

<br/>

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_metamorphe.jpg" width="300" height="100" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_nuage.jpg" width="300" height="100" />

<br/>

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_poche.jpg" width="300" height="100" />

<img src="https://gitlab.com/mutagene/mutagene/raw/master/material/plaques/plaque_precog.jpg" width="300" height="100" /> -->


* Pion régisseur R/V (1)

Le recto du pion indique le nombre de pions flaunes à placer dessus à chaque début de tour (5). Son verso est utilisé pour indiquer lorsque la fin de la partie est déclenchée.

* Pions clones (22) : Les pions spécifiques sont détaillés dans le guide stratégique

* Pions cuves (11)

* Pions PA (9)

* Pions Blessure (9)

* Pions objectifs (15)

* Pions flaune niveau 1 (15)

* Pion flaune niveau 2 (15)

* Pions puits (12)

* Pions extracteurs (21)

* Pions main (6)

* Pions bactérie (50)

* Cartes tribus (108) : 3 decks de 36 cartes (21 actions + 15 objectifs)

* Cartes zones (15)

* Cartes évènements (30)

* Cartes clones (81) : 7 cartes (4 mutations + 3 actions) par clone (à l'exception de la métamorphe (4 mutations))

* Guide stratégique des clones

* Livret de règles

## But du jeu

## Mise en place

(trophés)

### Surface de jeu

### Tribus

## Tour de jeu

### Début de tour

### Séquence

Une séquence commence par la désignation du clone actif et la réalisation d'une action (via carte ou pas, mais on ne peut pas commencer par jouer une carte ou un effet qui n'est pas une action, genre utiliser carte en génothèque pour pioche).

Fin de séquence :
- Possibilité de greffer une mutation à un clone en jeu (actif, allié ou adverse)
- Restaurer tout ses puits

## Plateaux

### Cases et Portée

### Evénements et Flaune

La pose du pion flaune se fait par le joueur actif.

Le pion régisseur a 5 cases sur le recto.

### Puits

### Conduits

### Cases Objectifs

## Cartes

Jouer une carte : uniquement pendant une de nos propores séquences.

### QG / AP

(à ignorer en league d'intro)

### Tribus

#### Action

#### Objectifs

Règle de base : Activez sur le pion objectif en jeu correspondant (même symbole et même nombre de points {PAO temporaire}) pour placer la carte objectif sur un emplacement objectif libre du clone actif. Placez le pion objectif sur la carte objectif.

Lorsqu'on valide un objectif, on gagne automatiquement les Bactéries qui s'y trouvent.

#### Main

#### Pioche

#### Défausse

#### Charnier

#### Génothèque

## Clones

### Pa

### Blessures

Soigner : décaler pion pa vers la gauche OU pion blessure vers la droite (le terme restarer n'existe plus)

Mort : objectif greffés défaussés, pions objectifs posé sur la case du clone. Si le clone mort n'est pas le clone actif, le joueur actif gagne trophé.

### Pion cuve

### Mutation

Greffe : fin de tour, peut être faite sur adverse du moment qu'emplacement libre. PAS CLONE ACTIF

## Actions

* Couleur d'action, portée, dépense de PA

* Sortir des cuves : 0 PA

* Mouvement

* Activation

* Mêlée

* Concentration

* Onde de choc

* Soin

* Nécrose

* Extraction

### Résumé

* vert
  - Concentration, 3 PA 0 portée (bio / terrestre), chaque réussite donne 1 dé en concentration.
  - Mêlée, 1 PA 1 portée (bio/terrestre), {V} dés SR {V} opposant, chaque réussite dépense 1 PA (ou fait 1 Blessure si plus de PA disponible), l'adversaire ne se défend pas mais lance lui aussi {M} Dés à SR {M} de la cible pour même effet.
* bleu
  - Soin, 3 PA, {B} portée terrestre, {B} Dés SR {B} ciblz
  - Onde de chox, 3 PA, {B} portée terrestre, {B} dés SR {B} cible, chaque réussite permet de déplacer 1 case terrestre/bio, mais fini si pioche événement / flaune.
* rouge
  - Nécrose, 3 PA, {R} portée bio, {R} dés SR {R} cible, la cible se défend, chaque réussite attaque restante une fois défense déduite inflige 1 Blessure.
  - Extraction, 1/2/3 PA (selon le pion extracteur posé), {R} portée bio, cible pion puits pour le remplacer par un pion extracteur (l'action n'est pas réalisable si la tribu a son maximum de puits)

### Évènement

## Fins du jeu

## Divers

Effet de carte "Soin X" -> au clone en jeu ciblé, pas forcément le clone actif

Trophée => bactérie sur clone

"Soin X" : soignez jusqu'à X sur ce clone (exemples POC3, POC6), ce n'est pas une action de soin

Case adjacente : case accessible par mouvement terrestre de 1.