import { expect } from 'chai'
import { changeName, changeRessources, getTribeOfCard, translateColors } from '../src/compilation/compilation'
import * as M from '../src/types/mutagene'


it('Change name', () => {
    expect(
        changeName('test')('test {{name}} test')
    ).equals('test test test')

    expect(
        changeName('test')('test {{bar}} test')
    ).not.equals('test test test')

    expect(
        changeName('test')('test {{{{name}}}} test')
    ).equals('test {{test}} test')

    expect(
        changeName('test')('test name test')
    ).not.equals('test test test')

  })


it('Translate colors works', () => {
    expect(translateColors('green')).to.eql('vert')
    expect(translateColors('blue')).to.eql('bleu')
    expect(translateColors('red')).to.eql('rouge')
    expect(translateColors('foo')).to.be.undefined
})

it('Change ressources works', () => {
    const dictionnary: {[x: string]: string} = {
      'major': 'blue',
    }
    expect(changeRessources(dictionnary)('{{major}}, major')).to.eql('bleu, major')
  })


it('GetTribeOfCard works', () => {
    const card: M.TribeCard = {
        id: 'F1',
        name: 'foo',
        type: 'tribe',
        set: 'tribe.toto',
        color: 'red',
        effects: []
    }

    expect(getTribeOfCard(card)).to.eqls('toto')
})

