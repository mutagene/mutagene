# Bâtiments

## Fonctionnement général

### QG

Règle qui donne un petit bonus à sa tribu, mais pas game-changing.

Valeur de base pour la main et les puits

Effet commun à tous les QG : "Burn objectif de la main : piochez 2 cartes".

### AP

Règle qui s'applique à tout le monde et change la dynamique de la partie sur un aspect (pas totalement révolutionnaire non plus, mais encourage/décourage telle approche).

(Éventuel) modificateur main / puits

Effet Burn objectif unique ? (peut être pas nécessaire, autant avoir un effet unique sur le QG directement, l'idée c'est de pouvoir burn objectif dès le début, pas d'être tenté d'attendre l'AP pour le faire)

## Idées d'effets

### QG

- Possibilité de passer (greffe ?) un objectif d'un clone à un autre (sous conditions d'emplacement libre) : à quel prix ? (juste greffe me parait un peu open bar, mais c'est QG après tout)
- Sur plateau avec 2 symboles : possibilité de considérer un symbole comme l'autre pour activation objectif (dans les 2 sens)
- Début du tour : vous pouvez défausser 1 carte couleur de votre clone de gauche pour piocher X cartes, X = nombre de cartes en main de la couleur de votre clone de droite.
- Vos Clones de couleur Bleu ont +1 SR aux nécroses les ciblant, (et -1 SR soin les ciblant) -> bref effet situationnel selon la composition de nos clones
- L'AP adverse n'a aucun effet spécial (à worder, mais en gros il a juste puits, main et burn)
- (pour phase de test : ) aucun effet, main à 6
- Possibilité d'avoir 2 AP : la troisième carte du draft est conservée, et on place ainsi les 2 QG sous la pioche. On gagne le premier avec le premier tour de pioche, et le second après un autre tour de pioche.
- Lorsque vous piochez votre AP, vous pouvez à la place choisir un AP non utilisé.

### AP

- Au début de chaque tour, chaque clone en jeu gagne son maximum de dés en concentration
- Lorsqu'un clone avec un objectif greffé meurt, son objectif est placé au sommet de la pioche de sa tribu.
- Au début de chaque tour, chaque joueur peut (doit ?) mélanger sa main et en repiocher une nouvelle.
- Au début de chaque tour, le joueur à la droite du régisseur peut prendre le pion régisseur pour piocher 2 cartes (ou : le régisseur peut donner le pion au joueur à sa droite pour piocher 2 cartes)
