# Génothèque

Les bâtiments donnent un nombre de cartes que l'on peut stocker en génothèque pour chaque couleur (exemple : 1V/1B/2R).

Toute carte de la main, à l'exception des objectifs, peut être payée son plein prix pour être posée en génothèque. L'effet de la carte n'est pas appliqué, on la pose face cachée dans notre génothèque (emplacement à côté du ou des Bâtiments, à définir).

Au début du tour, on retourne face visible toutes les cartes de toutes les génothèques.Une carte face visible en génothèque peut être utilisée comme une carte de la main pour une ressource de sa couleur, ou être jouée en ignorant son coût en ressources comme si elle était dans la main (pas d'effet de burn, on a payé l'effet de base et on y reste).

Ce système permet de temporiser des cartes pour libérer sa main, faire des conversions de Ressources, etc.

Des cartes peuvent avoir un effet différent (plus ou moins bon, fondamentalement différent, prix, etc) si jouées depuis la génothèque.
