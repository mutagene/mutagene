## Description
Mode coop avec matériel le plus compatible possible (au moins les clones et leur plaque).

Chaque joueur incarne un unique clone. En plus des mutations, un ensemble de cartes pour faire du build (équipements sûrement, mais on peut rester dans l'approche bio). Ces cartes s'assignent par face de dé : elles ont chacune un range (genre 1:2, 1:6, 5) qui détermine quelles faces ont peu choisir. Lors des jets de dés, on résoud dans l'ordre croissant : pouvoir "1", pouvoirs "2, etc. Cheque effet va permettre de modifier d'autres faces, de changer le SR, charger des cartes avec des jetons V-B-R par exemple. La majorité de ses effets sont cumulables en fonction du nombre de dés qui affichent la bonne face (exemple : "1" : ajoutez jeton V sur la carte ...), ou alors il faut en faire un certains nombre (2+ "2" : -1 SR)

Idée de dés de couleur avec des ranges différents. Peut être que le choix des dés est entièrement laissé aux joueurs, du moins pour leurs actions (et carac du monstre pour les monstres). Genre 1-2-2-3-3-6 / 1-3-3-4-4-6 / etc (avec genre moyenne égale, et toujours 1 et 6, ou au moins 1). ça permet d'éviter des faces de dés qu'on utilisera jamais dans nos slots ... Noir (classique) + V/B/R je pense.

Plein de combos possibles avec ce système.

Se joue par scénario, on peut imaginer des campagnes (succession de scénarios avec embranchement multiples), voire même un système de création (ou au moins de modif) de scénario.

Peut déboucher sur une autre variante avec overlord ?

Essayer d'insufler des dynamiques propres à chaque scénario : un où il faut runer vers la sortie, un où il faut runner vers l'antre pour le boss, un où il faut prendre son temps pour se préparer, etc.

Deck pour le 'boss' (quand il y en a un, mais ça sera le cas de figure classique je pense). Au dos des cartes, schéma qui permet de connecter des cartes entre elles. Au fur et à mesure de la partie, on construit une sorte d'arbre avec ces cartes. Lorsqu'on arrive dans la salle du boss, toutes ses cartes dorme son IA (certaines en pioche, d'autres en fixe ? à voir). L'enjeu de la partie sera de se préparer au mieux au fur et à mesure que ces cartes arrivent, voir d'essayer d'agir dessus (tri, purge, etc).

Une des dynamique de base serait de forcer les joueurs à soit rusher vers l'inconnu, soit prendre le temps de choisir ses outils, de "farmer", "fouiller", etc mais au prix d'une menace plus forte à affronter. Typiquement le boss de fin croît tranquillement dans son coin pendant que les joueurs explorent, essayent de rassembler des infos sur lui, cherchent des armes, etc.

