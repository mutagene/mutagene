# Clones

Effet plaque : résistances aux greffe forcée (Insectoïde ?)

action : +3 dés en concentration en ignorant max, burn : payez carte avec dés en concentration

# Objectifs, Plateaux et Bâtiments

Les plateaux sont recto-versos, avec parfois les mêmes symboles d'objectifs/bâtiments, parfois 1 symbole en commun et 1 qui change, parfois tous différents, etc ...
Si symboles différents d'une face à l'autre, on peut en profiter pour mettre en place des systèmes de règles incompatibles (exemple : un seul déploiement / 3 déploiements) ou au moins qui interagissent moins bien. Inversement on peut juste proposer un petit changement d'une face à l'autre avec 2 bâtiments communs et un propre à chaque face qui vient tout perturber.

