## Clone Corrompu

Le joueur Organisme dispose d'un clone "corrompu" (verso de la plaque clone) choisi en début de partie pendant la [Mise en place](https://app.nuclino.com/t/b/6d2f49a0-0883-4882-906c-d8b08a5e2901) :

Quelles caractéristiques sur ce verso ? Idem verso ?

Pour agir, le clone corrompu dispose-t-il de PA ou y a-t-il un système d'actions propres à l'Organisme (type petit deck) ?

Qu'en est-il de ses cartes propres (actions/mutations) ?

D'ailleurs, le clone corrompu entre-il en jeu en début de partie ou à partir d'un certain nombre de pions flaune en jeu (pions qui pourraient servir de déploiement) ? Du coup d'ici là, le joueur Organisme dispose de petits moyens de pressions via des cartes (type nuée de criquets).

1 unique clone + une manipulation de la flaune (que ce soit incarné par des pions flaunes ou pas) qui passe potentiellement par des pions "minions" / "plantes" / ..., via des mutations propres peut être.

J'en suis venu un peu à la même conclusion : Un unique clone et un choix d'une ou deux créatures mineures parmi un deck propre de 5/6, ainsi que des mutations organisme. Pour pallier au fait que le clone est seul face à 3, il pourrait bénéficier d'un bonus en défense (de type + 1 dé et/ou +1 SR).

Le joueur Organisme pourrait utiliser les pions flaunes sur le plateau comme ressources (avec un ratio genre 2 pions flaunes = 1 ressources). Pions qui seraient retirés du plateau.

# Dynamique

Est-ce qu'on va basculer dans du "tous contre un" ou est-ce que ça va rester chacun pour soit ? EN ajoutant un joueur lié à l'organisme, on se doute bien qu'il va constituer une cible/menace pour les autres joueurs, mais les tribus continuent d'avoir leurs objectifs et à s'affronter, ou on propose un mode différent pour les tribus ?

Ça serait dommage de proposer un mode à part.. Alors que l'Organisme constitue plus un façon différente de jouer plutôt qu'un ennemi à part.

Je me met à la place de ces cons de joueurs à qui on apprends que l'organisme c'est vilain. Du coup le jour où ils se retrouvent face à un joueur organisme, quelque soit la règle, ils risque de se mettre à tous lui taper dessus. Sans aller jusqu'à proposer un mode de jeu à part, on peut essayer d'anticiper cette dynamique, tout simplement en assumant et en rendant le joueur organisme plus fort de base.\
Petit doute aussi sur les cartes de tribus : est-ce qu'elles ne vont pas naturellement donner que des avantages contre un joueur tribu et beaucoup moins s'appliquer à un joueur organisme ? Il faut le prévoir tôt pour éviter ce soucis (par exemple définir si le terme de tribu s'applique au joueur organisme, sinon ça nique "la tribu ciblée ...").

D’où le +1 dé et/ou +1 SR.. Question de sémantique, oué, c'est ce qui m"inquiète le moins ( même si faut l'anticiper, on est d'accord)

Pour les objectifs de l'Organisme, (attention, tu vas kiffer (INDEED)) je propose des cartes (même pas forcément mini) que le clone corrompu va poser sur les plateaux pour modifier les cases et ajouter une petite règle propre au plateau corrompu. Avec gains de bactéries par plateaux corrompus. → interaction possible avec la règle de "la récolte retire un pion flaune", ça peut être intéressant de creuser.

Idée : une piste qui pourrait être cool serait de justement bien bien inscrire la géographie dans le jeu de l'Organisme : pose de pions, contamination de plateaux les uns après les autres, etc. Peut être un plateau de départ comme avant pour les tribus, voire un positionnement différent de la surface de jeu pour rendre l'organisme plus central (genre 1 plateau au milieu et 4 autour) ...

Idée background : la flaune "piège" ou "pirate" une borne de clonage et y attend tranquillement qu'un clone s'y connecte pour lui piquer sa génotype et du coup en prend le contrôle (ici génotype = individu, vu que l'idée c'est qu'il y en ai qu'un à tout moment, mais qu'un peu re créer facilement).

Idée : joueur organisme commence sans clone (ou 1 mais ça marche aussi, à voir), et dès qu'un clone "tribu" meurt, le joueur organisme en récupère la plaque face "pervertie", soigne, met les PA à fond, et rend le précédent clone (si y'a et si pas un neutre qu'on lui donnerai en début de partie), qui revient avec full PA et pas de blessure également. Le clone garde la même position pour sa cuve quelque soit le côté. Du coup pas de notion de cuve, pas besoin de déploiement une fois tout le monde arrivé. Par contre dynamique super chelou de jeu donc à bien réfléchir (du genre l'organisme peut difficilement avoir des objectifs à faire en plusieurs phases sur le plateau en se trimballant la carte objectif \[ou alors si mais du coup l'objectif reste attaché au clone qui ne peut pas s'en débarrasser et sait que l'organisme va vouloir le tuer pour le finir {ou alors on distribue un objectif organisme à chaque clone en début de partie mais ça leur bloque pas de slot et aucun moyen d"en gagner ou de l'enlever hormis pour l'organisme qui peut les valider. il a donc un pool de objectifs/clones à aller parasiter et scorer ...}\]). Est-ce que pion cuve est déplacé avec la même règle ou est ce qu'il y a possibilité d'introduire une variante (tous les pouvoirs sont actif par exemple, mais ça empêche l'idée de pouvoir incompatibles) ?

Je verrais bien un deck organisme où chaque carte, plutôt que d'être un modificateur d'action (comme pour les decks tribus) serait une action (je pousserai même le vice jusqu'à aller mettre les déplacements et activations dans ce deck). De cette manière, il n'y a plus besoin de PA pour le clone corrompu. Si je veux vraiment pousser le bouchon, je dirai même qu'il pourrait ne pas avoir non plus ni pion cuve ni pion blessure. Oui, tu as bien lu, un clone 'immortel' qui donc ne serait plus une cible privilégié pour les autres joueurs (what the point ?). En même temps, le fait que ses actions soient soumises à la pioche ne le rends pas non plus tout puissant. Un peu comme un marionnette en quelque sorte. Dans ce deck, il pourrait également y avoir tout un tas de créatures (avec des pions correspondants pour le plateau). Ces créatures pourraient remplacer les mutations (une créature est sous le contrôle du clone corrompu qui ne peut pas en contrôler plus qu'il n'a de slots disponibles).

En ce qui concerne le déploiement, il n'y a aucune raison (après réflexion) que l'organisme ne choisisse pas également un bâtiment en début de partie lors du draft. Il lui suffit ensuite d'aller chercher l'objectif (carte qui modifie le plateau) correspondant et de le poser à sa place ! Et BIM !


### Notes

corruption plateau : conditions peuvent être du fait des tribus pour leur permettre de niquer un clone qui va faire un objectif

Mise en place, lorsqu'on prend son premier clone on peut le mettre face organisme et jouer l'organisme

cf. T4 : on implique que l'organisme ne puisse pas devenir régisseur

Est ce qu'il a une taille de main ? #E22