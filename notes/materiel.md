# Générique

* 30 pions flaunes ***{20 à deux joueurs ?}***
* 15 cartes bâtiments (de 2 à 3 par plateau, très variable avec l'ajout de plateaux)
* 12 pions puits (4 de chaque couleur) + réserve ?

# Factions

* 7 Pions extracteurs ([cf. Ressources](./regles/ressources.md)) : 1 niv. 1, 5 niv. 2 et 1 niv. 3
* 21 cartes "de base" (10 primaire, 7 secondaires, 4 tertiaires)
* 15 cartes objectif (en l'état des plateaux existants : 6/5/4)

# Clones

* ***4*** Cartes mutations (1 de chaque + 1 de sa couleur)
* ***3*** Cartes actions (3 à sa couleur)
* 1 pion à son effigie
* 1 plaque
* 1 pion PA (générique)
* 1 pion Blessure (générique)
* 1 pion Cuve
