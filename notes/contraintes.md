Aucun clone ne peut agir directement sur les événements (éventuellement éviter la pioche seulement) : réservé à l'Organisme.

Aucun clone ne *devrait* avoir d'effet liés directement à des éléments de jeu propre au mode de jeu "escarmouche" (missions, puits, bâtiments)