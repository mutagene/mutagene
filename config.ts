module.exports = () => ({
  app_name: 'mutagene',
  scope: 'global',


  // path
  path: {

    tts: './tts',

    material_assets: './material',
    individual_yml_export: './individual_yml_export',
    // personnal tts save folder, (relative path from mutagene folder for now, may change with an more elegant way to target HOME)
    tts_save: '~/.local/share/Tabletop Simulator/Saves/',

    // tts root path (distant or local)
    /*
    tts_root: `file:////home/{mutagene}/material/`,
    /*/
    tts_root: `https://gitlab.com/mutagene/mutagene/raw/master/material/`
    //*/
  },

})