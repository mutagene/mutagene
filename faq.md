# FAQ

## Cartes

- T24, T25 :
  - Ne se valident pas automatiquement à la fin de la partie.
  - Si ces  objectifs sont greffés l'un sur l'autre lors de la mort d'une clone, la tribu propriétaire des objectifs choisit l'ordre de résolution (T24 avant T25 pour profiter des bactéries des deux cartes).

- LIQ4 : pas points, sur potion, on ne regarde pas la couleur, juste le symbole.

Pose de flaune : si plus d'emplacement adjacent, sur autre grappe de flaune